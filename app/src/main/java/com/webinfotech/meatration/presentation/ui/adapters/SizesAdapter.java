package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.Size;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SizesAdapter extends RecyclerView.Adapter<SizesAdapter.ViewHolder> {

    public interface Callback {
        void onSizeSelect(int position);
    }
    Context mContext;
    Size[] sizes;
    Callback mCallback;

    public SizesAdapter(Context mContext, Size[] sizes, Callback mCallback) {
        this.mContext = mContext;
        this.sizes = sizes;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_sizes, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (sizes[position].sizeTypeId == 1) {
            holder.txtViewSize.setText(sizes[position].size + " kg");
        } else {
            holder.txtViewSize.setText(sizes[position].size + " lt");
        }
        if (sizes[position].isSelected) {
            holder.greyView.setVisibility(View.GONE);
            holder.accentView.setVisibility(View.VISIBLE);
        } else {
            holder.greyView.setVisibility(View.VISIBLE);
            holder.accentView.setVisibility(View.GONE);
        }
        holder.greyView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSizeSelect(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sizes.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_size)
        TextView txtViewSize;
        @BindView(R.id.grey_view)
        View greyView;
        @BindView(R.id.accent_view)
        View accentView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(Size[] sizes) {
        this.sizes = sizes;
        notifyDataSetChanged();
    }

}
