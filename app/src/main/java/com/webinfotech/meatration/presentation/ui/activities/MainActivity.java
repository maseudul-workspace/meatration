package com.webinfotech.meatration.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.domain.models.testing.Category;
import com.webinfotech.meatration.domain.models.testing.Image;
import com.webinfotech.meatration.domain.models.testing.Product;
import com.webinfotech.meatration.presentation.presenters.MainPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.meatration.presentation.ui.adapters.CategoryAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.HomeViewpagerAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.ProductsHorizontalAdapter;
import com.webinfotech.meatration.threading.MainThreadImpl;
import com.webinfotech.meatration.util.GlideHelper;

import java.util.ArrayList;
import java.util.concurrent.Executor;

public class MainActivity extends BaseActivity implements MainPresenter.View {

    @BindView(R.id.viewpager1)
    ViewPager viewPager1;
    @BindView(R.id.viewpager2)
    ViewPager viewPager2;
    @BindView(R.id.viewpager1_dots_layout)
    LinearLayout viewpager1DotsLayout;
    @BindView(R.id.viewpager2_dots_layout)
    LinearLayout viewpager2DotsLayout;
    @BindView(R.id.recycler_view_category)
    RecyclerView recyclerViewCategory;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.layout_location)
    View locationLayout;
    @BindView(R.id.recycler_view_new_products)
    RecyclerView recyclerViewNewProducts;
    @BindView(R.id.recycler_view_popular_products)
    RecyclerView recyclerViewPopularProducts;
    @BindView(R.id.view_popular_products)
    View viewPopularProducts;
    @BindView(R.id.view_new_products)
    View viewNewProducts;
    MainPresenterImpl mPresenter;
    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.fetchMainData();
    }

    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void prepareViewpager1DotsIndicator(int sliderPosition, int length){
        if(viewpager1DotsLayout.getChildCount() > 0){
            viewpager1DotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            viewpager1DotsLayout.addView(dots[i], layoutParams);

        }
    }

    private void prepareViewpager2DotsIndicator(int sliderPosition, int length){
        if(viewpager2DotsLayout.getChildCount() > 0){
            viewpager2DotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            viewpager2DotsLayout.addView(dots[i], layoutParams);

        }
    }

    private void setNestedScrollViewListener() {
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    locationLayout.animate().translationY(locationLayout.getHeight()).setInterpolator(
                            new DecelerateInterpolator(2)
                    ).start();
                }
                if (scrollY < oldScrollY) {
                    locationLayout.animate().translationY(0).setInterpolator(
                            new DecelerateInterpolator(2)
                    ).start();
                }
            }
        });
    }

    @OnClick(R.id.layout_location) void onLocationLayoutClicked() {
        Intent intent = new Intent(this, LocationPickActivity.class);
        startActivity(intent);
    }

    @Override
    public void loadData(HomeViewpagerAdapter adapter, CategoryAdapter categoryAdapter, int sliderLength, ProductsHorizontalAdapter newProductsAdapter, ProductsHorizontalAdapter popularProductsAdapter, int newProductsLength, int popularProductsLength) {
        recyclerViewCategory.setAdapter(categoryAdapter);
        recyclerViewCategory.setLayoutManager(new GridLayoutManager(this, 3));

        prepareViewpager1DotsIndicator(0, sliderLength);
        prepareViewpager2DotsIndicator(0, sliderLength);

        viewPager1.setAdapter(adapter);
        viewPager2.setAdapter(adapter);

        viewPager1.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareViewpager1DotsIndicator(position, sliderLength);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager2.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareViewpager2DotsIndicator(position, sliderLength);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        recyclerViewPopularProducts.setAdapter(popularProductsAdapter);
        recyclerViewPopularProducts.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.HORIZONTAL, false));

        recyclerViewNewProducts.setAdapter(newProductsAdapter);
        recyclerViewNewProducts.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.HORIZONTAL, false));

        Log.e("LogMsg", "New Products Length: " + newProductsLength);
        Log.e("LogMsg", "Popular Products Length: " + popularProductsLength);

        if (newProductsLength == 0) {
            recyclerViewNewProducts.setVisibility(View.GONE);
            viewNewProducts.setVisibility(View.GONE);
        }

        if (popularProductsLength == 0) {
            recyclerViewPopularProducts.setVisibility(View.GONE);
            viewPopularProducts.setVisibility(View.GONE);
        }

    }

    @Override
    public void onCategoryClicked(int categoryId, int isSubCategory) {
        if (isSubCategory == 1) {
            Intent intent = new Intent(this, ProductListActivity.class);
            intent.putExtra("categoryId", categoryId);
            intent.putExtra("type", 1);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, SubcategoryActivity.class);
            intent.putExtra("categoryId", categoryId);
            startActivity(intent);
        }
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void onProductClicked(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }
}