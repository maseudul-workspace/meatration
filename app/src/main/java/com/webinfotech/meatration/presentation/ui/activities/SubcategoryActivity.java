package com.webinfotech.meatration.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.domain.models.testing.Category;
import com.webinfotech.meatration.domain.models.testing.SubcategoryViewpager;
import com.webinfotech.meatration.presentation.presenters.SubcategoryPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.SubcategoryPresenterImpl;
import com.webinfotech.meatration.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.SubcategoryViewpagerAdapter;
import com.webinfotech.meatration.threading.MainThreadImpl;
import com.webinfotech.meatration.util.GlideHelper;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SubcategoryActivity extends AppCompatActivity implements SubcategoryPresenter.View {

    @BindView(R.id.recycler_view_subcategory)
    RecyclerView recyclerViewSubcategory;
    @BindView(R.id.txt_view_category_name)
    TextView txtViewCategoryName;
    @BindView(R.id.txt_view_category_desc)
    TextView txtViewCategoryDesc;
    @BindView(R.id.img_view_category)
    ImageView imgViewCategory;
    SubcategoryPresenterImpl mPresenter;
    int categoryId;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);
        ButterKnife.bind(this);
        categoryId = getIntent().getIntExtra("categoryId", 0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Grocery");
        setCategoryInfo();
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchSubcategories(categoryId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new SubcategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setCategoryInfo() {
        GlideHelper.setImageView(this, imgViewCategory, "https://www.pngmart.com/files/7/Grocery-Transparent-Background.png");
        Animation rightToLeft1 = AnimationUtils.loadAnimation(this, R.anim.right_to_left);
        Animation rightToLeft2 = AnimationUtils.loadAnimation(this, R.anim.right_to_left);
        Animation leftToRight = AnimationUtils.loadAnimation(this, R.anim.left_to_right);
        leftToRight.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                txtViewCategoryName.setVisibility(View.VISIBLE);
                txtViewCategoryName.setAnimation(rightToLeft1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        rightToLeft1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                txtViewCategoryDesc.setVisibility(View.VISIBLE);
                txtViewCategoryDesc.setAnimation(rightToLeft2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imgViewCategory.setAnimation(leftToRight);
    }

    @Override
    public void loadData(SubcategoryAdapter adapter) {
        recyclerViewSubcategory.setAdapter(adapter);
        recyclerViewSubcategory.setLayoutManager(new GridLayoutManager(this, 3));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onSubcategoryClicked(int subcategoryId) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", subcategoryId);
        intent.putExtra("type", 2);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}