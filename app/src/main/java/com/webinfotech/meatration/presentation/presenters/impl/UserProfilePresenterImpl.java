package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.meatration.domain.interactors.UpdateUserInteractor;
import com.webinfotech.meatration.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.UpdateUserInteractorImpl;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.presentation.presenters.UserProfilePresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class UserProfilePresenterImpl extends AbstractPresenter implements  UserProfilePresenter,
                                                                            FetchUserProfileInteractor.Callback,
                                                                            UpdateUserInteractor.Callback

{

    Context mContext;
    UserProfilePresenter.View mView;
    AndroidApplication androidApplication;
    FetchUserProfileInteractorImpl fetchUserProfileInteractor;
    UpdateUserInteractorImpl updateUserInteractor;

    public UserProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchUserProfileInteractor = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken);
            fetchUserProfileInteractor.execute();
            mView.showLoader();
        }

    }

    @Override
    public void updateProfile(String name, String email, String mobile, String DOB, String gender, String city, String state, String pin, String address) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            updateUserInteractor = new UpdateUserInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken, name, email, mobile, DOB, gender, state, city, pin, address);
            updateUserInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingUserProfileSuccess(UserInfo userInfo) {
        mView.hideLoader();
        mView.loadUserProfileData(userInfo);
    }

    @Override
    public void onGettingUserProfileFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onUserUpdateSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "User Updated Successfully").show();
    }

    @Override
    public void onUserUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
