package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.Product;
import com.webinfotech.meatration.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsHorizontalAdapter extends RecyclerView.Adapter<ProductsHorizontalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
    }

    Context mContext;
    Product[] products;
    Callback mCallback;

    public ProductsHorizontalAdapter(Context mContext,Product[] products, Callback mCallback) {
        this.mContext = mContext;
        this.products = products;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_products_horizontal, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/images/products/" + products[position].mainImage);
        holder.txtViewProductName.setText(products[position].name);
        holder.txtViewPrice.setText("₹ " + products[position].minPrice);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
