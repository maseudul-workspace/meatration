package com.webinfotech.meatration.presentation.presenters;

public interface ChangePasswordPresenter {
    void changePassword(String oldPassword, String newPassword);
    interface View {
        void showLoader();
        void hideLoader();
    }
}
