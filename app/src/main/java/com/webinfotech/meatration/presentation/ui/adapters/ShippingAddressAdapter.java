package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.ShippingAddress;
import com.webinfotech.meatration.domain.models.testing.Address;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShippingAddressAdapter extends RecyclerView.Adapter<ShippingAddressAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int id);
        void onDeleteClicked(int id);
    }

    Context mContext;
    ShippingAddress[] addresses;
    Callback mCallback;

    public ShippingAddressAdapter(Context mContext, ShippingAddress[] addresses, Callback mCallback) {
        this.mContext = mContext;
        this.addresses = addresses;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_shipping_address, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewUsername.setText(addresses[position].name);
        holder.txtViewAddress.setText(addresses[position].address);
        holder.txtViewHouseNo.setText(addresses[position].city);
        holder.txtViewMobile.setText("Mobile - " + addresses[position].mobile);
        holder.txtViewStreet.setText("Postal Code - " + addresses[position].pin);
        holder.menuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.menuLayout);
                //inflating menu from xml resource
                popup.inflate(R.menu.popup_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.address_edit:
                                mCallback.onEditClicked(addresses[position].id);
                                break;
                            case R.id.address_delete:
                                mCallback.onDeleteClicked(addresses[position].id);
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return addresses.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_username)
        TextView txtViewUsername;
        @BindView(R.id.txt_view_address)
        TextView txtViewAddress;
        @BindView(R.id.txt_view_mobile)
        TextView txtViewMobile;
        @BindView(R.id.txt_view_street)
        TextView txtViewStreet;
        @BindView(R.id.txt_view_house_no)
        TextView txtViewHouseNo;
        @BindView(R.id.menu_layout)
        View menuLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
