package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.domain.models.ShippingAddress;

public interface EditAddressPresenter {
    void fetchAddressDetails(int addressId);
    void updateAddress(int addressId,
                       String name,
                       String email,
                       String mobile,
                       String city,
                       String state,
                       String pin,
                       String address,
                       double latitude,
                       double longitude);
    interface View {
        void loadAddressDetails(ShippingAddress address);
        void showLoader();
        void hideLoader();
    }
}
