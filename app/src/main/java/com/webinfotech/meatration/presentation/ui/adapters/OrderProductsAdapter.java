package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.OrderProducts;
import com.webinfotech.meatration.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderProductsAdapter extends RecyclerView.Adapter<OrderProductsAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId, int type);
    }

    Context mContext;
    OrderProducts[] orderProducts;
    Callback mCallback;

    public OrderProductsAdapter(Context mContext, OrderProducts[] orderProducts, Callback mCallback) {
        this.mContext = mContext;
        this.orderProducts = orderProducts;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_order_products, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductName.setText(orderProducts[position].productName);
        holder.txtViewMrp.setText("Rs. " + orderProducts[position].mrp);
        holder.txtViewPrice.setText("Rs " + orderProducts[position].price);
        holder.txtViewQty.setText(Integer.toString(orderProducts[position].quantity));
        holder.txtViewSize.setText(orderProducts[position].size);
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/images/products/" + orderProducts[position].productImage);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(orderProducts[position].productId, orderProducts[position].productType);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderProducts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_size)
        TextView txtViewSize;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQty;
        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
