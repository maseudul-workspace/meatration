package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.domain.models.PaymentData;

public interface PaymentPresenter {
    void placeOrder(int deliveryType, int paymentType, int shippingAddressId);
    void verifyPayment( String razorpayOrderId,
                        String razorpayPaymentId,
                        String razorpaySignature,
                        int orderId);
    interface View {
        void goToOrderHistory();
        void initiatePayment(PaymentData paymentData, int orderId);
        void onPaymentVerificationSuccess();
        void showLoader();
        void hideLoader();
    }
}
