package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.testing.ExpiryDate;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CardExpiryMonthAdapter extends RecyclerView.Adapter<CardExpiryMonthAdapter.ViewHolder> {

    public interface Callback {
        void onExpiryMonthClicked(int position);
    }

    Context mContext;
    ArrayList<ExpiryDate> dates;
    Callback mCallback;

    public CardExpiryMonthAdapter(Context mContext, ArrayList<ExpiryDate> dates, Callback mCallback) {
        this.mContext = mContext;
        this.dates = dates;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_card_expiry_dates, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewExpiryDate.setText(dates.get(position).appearedDate);
        holder.txtViewExpiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onExpiryMonthClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dates.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_expiry_date)
        TextView txtViewExpiryDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
