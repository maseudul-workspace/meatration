package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchCartListInteractor;
import com.webinfotech.meatration.domain.interactors.FetchChargesListInteractor;
import com.webinfotech.meatration.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.meatration.domain.interactors.RemoveCartItemInteractor;
import com.webinfotech.meatration.domain.interactors.UpdateCartInteractor;
import com.webinfotech.meatration.domain.interactors.impl.FetchCartListInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.FetchChargesListInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.RemoveCartItemInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.UpdateCartInteractorImpl;
import com.webinfotech.meatration.domain.models.CartList;
import com.webinfotech.meatration.domain.models.Charges;
import com.webinfotech.meatration.domain.models.ShippingAddress;
import com.webinfotech.meatration.domain.models.Size;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.presentation.presenters.CartDetailsPresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.presentation.ui.adapters.CartListAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class CartDetailsPresenterImpl extends AbstractPresenter implements  CartDetailsPresenter,
                                                                            CartShippingAddressAdapter.Callback,
                                                                            FetchShippingAddressInteractor.Callback,
                                                                            FetchCartListInteractorImpl.Callback,
                                                                            CartListAdapter.Callback,
                                                                            UpdateCartInteractor.Callback,
                                                                            RemoveCartItemInteractor.Callback,
                                                                            FetchChargesListInteractor.Callback
{

    Context mContext;
    CartDetailsPresenter.View mView;
    FetchShippingAddressInteractorImpl fetchShippingAddressInteractor;
    ShippingAddress[] shippingAddresses;
    CartShippingAddressAdapter cartShippingAddressAdapter;
    FetchCartListInteractorImpl fetchCartListInteractor;
    UpdateCartInteractorImpl updateCartInteractor;
    RemoveCartItemInteractorImpl removeCartItemInteractor;

    public CartDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCartDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchCartListInteractor = new FetchCartListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchCartListInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void fetchShippingAddressList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchShippingAddressInteractor.execute();
        }
    }

    @Override
    public void fetchChargesList() {
        FetchChargesListInteractorImpl fetchChargesListInteractor = new FetchChargesListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchChargesListInteractor.execute();
    }

    @Override
    public void onGettingAddressListSucces(ShippingAddress[] shippingAddresses) {
        this.shippingAddresses = shippingAddresses;
        cartShippingAddressAdapter = new CartShippingAddressAdapter(mContext, this.shippingAddresses, this);
        mView.loadShippingAddressAdapter(cartShippingAddressAdapter);
    }

    @Override
    public void onGettingAddressListFail(String errorMsg, int loginError) {

    }

    @Override
    public void onAddressSelected(int id) {
        for (int i = 0; i < shippingAddresses.length; i++) {
            if (shippingAddresses[i].id == id) {
                shippingAddresses[i].isSelected = true;
            } else {
                shippingAddresses[i].isSelected = false;
            }
        }
        cartShippingAddressAdapter.updateDataSet(this.shippingAddresses);
        mView.onAddressSelected(id);
    }

    @Override
    public void onEditClicked(int id) {
        mView.onShippingAddressEditClicked(id);
    }

    @Override
    public void onDeliverButtonClicked() {
        mView.goToPaymentActivity();
    }

    @Override
    public void onGettingCartListSuccess(CartList[] cartLists) {
        if (cartLists.length == 0) {
            mView.hideViews();
        } else {
            double total = 0;
            double discount = 0;
            double subTotal = 0;
            for (int position = 0; position < cartLists.length; position++) {
                Size[] sizes = cartLists[position].product.sizes;
                if (cartLists[position].product.productType == 2) {
                    if (cartLists[position].quantity > 6) {
                        for (int i = 0; i < sizes.length; i++) {
                            if (sizes[i].size == 6) {
                                total = total + sizes[i].price * cartLists[position].quantity;
                                subTotal = subTotal + sizes[i].mrp * cartLists[position].quantity;
                                discount = subTotal - total;
                                break;
                            }
                        }
                    } else {
                        for (int i = 0; i < sizes.length; i++) {
                            if (sizes[i].size == cartLists[position].quantity) {
                                total = total + sizes[i].price * cartLists[position].quantity;
                                subTotal = subTotal + sizes[i].mrp * cartLists[position].quantity;
                                discount = subTotal - total;
                                break;
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < sizes.length; i++) {
                        if (sizes[i].id == cartLists[position].sizeId) {
                            total = total + sizes[i].price * cartLists[position].quantity;
                            subTotal = subTotal + sizes[i].mrp * cartLists[position].quantity;
                            discount = subTotal - total;
                            break;
                        }
                    }
                }
            }
            CartListAdapter adapter = new CartListAdapter(mContext, cartLists, this);
            mView.loadCartAdapter(adapter, total, discount, subTotal);
        }
        if (cartLists.length == 1 && cartLists[0].quantity > 6) {
            mView.hideExpressDeliveryOption();
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingCartListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.hideViews();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void updateCart(int quantity, int cartId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            updateCartInteractor = new UpdateCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, cartId, quantity);
            updateCartInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void removeCart(int cartId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            removeCartItemInteractor = new RemoveCartItemInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, cartId);
            removeCartItemInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onCartUpdateSuccess() {
        Toasty.success(mContext, "Updated Successfully").show();
		fetchCartDetails();
    }

    @Override
    public void onCartUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onCartItemRemoveSuccess() {
        fetchCartDetails();
        Toasty.success(mContext, "Item Removed Successfully").show();
    }

    @Override
    public void onCartItemRemoveFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onChargesListFetchSuccess(Charges[] charges) {
        mView.loadCharges(charges);
    }

    @Override
    public void onChargesListFetchFail() {

    }
}
