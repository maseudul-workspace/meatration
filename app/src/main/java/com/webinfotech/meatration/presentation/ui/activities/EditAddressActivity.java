package com.webinfotech.meatration.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.domain.models.ShippingAddress;
import com.webinfotech.meatration.presentation.presenters.EditAddressPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.EditAddressPresenterImpl;
import com.webinfotech.meatration.threading.MainThreadImpl;

public class EditAddressActivity extends AppCompatActivity implements EditAddressPresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPin;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    ProgressDialog progressDialog;
    EditAddressPresenterImpl mPresenter;
    int addressId;
    double latitude;
    double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        getSupportActionBar().setTitle("Edit Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        addressId = getIntent().getIntExtra("addressId", 0);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchAddressDetails(addressId);
    }

    private void initialisePresenter() {
        mPresenter = new EditAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_view_update_address) void onUpdateAddressClicked() {
        if (    editTextName.getText().toString().trim().isEmpty() ||
                editTextEmail.getText().toString().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty() ||
                editTextCity.getText().toString().trim().isEmpty() ||
                editTextPin.getText().toString().trim().isEmpty() ||
                editTextState.getText().toString().trim().isEmpty() ||
                editTextAddress.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(this, "Please fill all the fields").show();
        } else {
            mPresenter.updateAddress(
                    addressId,
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextPhone.getText().toString(),
                    editTextCity.getText().toString(),
                    editTextState.getText().toString(),
                    editTextPin.getText().toString(),
                    editTextAddress.getText().toString(),
                    latitude,
                    longitude
            );
        }
    }

    @Override
    public void loadAddressDetails(ShippingAddress address) {
        editTextAddress.setText(address.address);
        editTextCity.setText(address.city);
        editTextEmail.setText(address.email);
        editTextName.setText(address.name);
        editTextPhone.setText(address.mobile);
        editTextPin.setText(address.pin);
        editTextState.setText(address.state);
        this.latitude = address.latitude;
        this.longitude = address.longitude;
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}