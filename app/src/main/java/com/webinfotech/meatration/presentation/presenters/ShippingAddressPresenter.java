package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.presentation.ui.adapters.ShippingAddressAdapter;

public interface ShippingAddressPresenter {
    void fetchShippingAddress();
    interface View {
        void loadAdapter(ShippingAddressAdapter adapter);
        void goToAddressDetails(int addressId);
        void onAddressFetchFailed();
        void showLoader();
        void hideLoader();
    }
}
