package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;


import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.AddShippingAddressInteractor;
import com.webinfotech.meatration.domain.interactors.impl.AddShippingAddressInteractorImpl;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.presentation.presenters.AddAddressPresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class AddAddressPresenterImpl extends AbstractPresenter implements AddAddressPresenter, AddShippingAddressInteractor.Callback {

    Context mContext;
    AddAddressPresenter.View mView;
    AddShippingAddressInteractorImpl addShippingAddressInteractor;

    public AddAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void addAddress(String name, String email, String mobile, String city, String state, String pin, String address,  double latitude, double longitude) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            addShippingAddressInteractor = new AddShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken, name, email, mobile, state, city, pin, address, latitude, longitude);
            addShippingAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onAddShippingAddressSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Address Added Successfully").show();
        mView.onAddAddressSuccess();
    }

    @Override
    public void onAddShippingAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
