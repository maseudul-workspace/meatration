package com.webinfotech.meatration.presentation.presenters;

public interface AddAddressPresenter {
    void addAddress(String name,
                    String email,
                    String mobile,
                    String city,
                    String state,
                    String pin,
                    String address,
                    double latitude,
                    double longitude);
    interface View {
        void onAddAddressSuccess();
        void showLoader();
        void hideLoader();
    }
}
