package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.presentation.ui.adapters.ProductsVerticalAdapter;

public interface ProductListPresenter {
    void fetchProductList(int categoryId, int type, int page);
    void fetchWishList();
    interface View {
        void loadAdapter(ProductsVerticalAdapter adapter, int totalPage);
        void onProductClicked(int productId);
        void showLoader();
        void hideLoader();
        void fetchProductList();
        void showLoginBottomSheet();
    }
}
