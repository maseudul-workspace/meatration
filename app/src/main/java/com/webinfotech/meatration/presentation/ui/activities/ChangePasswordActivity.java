package com.webinfotech.meatration.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.presentation.presenters.ChangePasswordPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.ChangePasswordPresneterImpl;
import com.webinfotech.meatration.threading.MainThreadImpl;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordPresenter.View {

    @BindView(R.id.txt_input_current_password_layout)
    TextInputLayout txtInputOldPassword;
    @BindView(R.id.txt_input_new_password_layout)
    TextInputLayout txtInputNewPassword;
    @BindView(R.id.txt_input_confirm_password_layout)
    TextInputLayout txtInputConfirmPassword;
    @BindView(R.id.edit_text_current_password)
    EditText editTextOldPassword;
    @BindView(R.id.edit_text_new_password)
    EditText editTextNewPassword;
    @BindView(R.id.edit_text_confirm_password)
    EditText editTextConfirmPassword;
    ChangePasswordPresneterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ChangePasswordPresneterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_change_password) void onChangePasswordClicked() {
        txtInputConfirmPassword.setError("");
        txtInputNewPassword.setError("");
        txtInputOldPassword.setError("");
        if (editTextOldPassword.getText().toString().trim().isEmpty()) {
            txtInputOldPassword.setError("Please Enter Your Current Password");
        } else if (editTextNewPassword.getText().toString().trim().isEmpty()) {
            txtInputNewPassword.setError("Please Enter New Password");
        } else if (editTextConfirmPassword.getText().toString().trim().isEmpty()) {
            txtInputConfirmPassword.setError("Please Confirm Password");
        } else if (!editTextNewPassword.getText().toString().trim().equals(editTextConfirmPassword.getText().toString().trim())) {
            Toasty.warning(this, "Password Mismatch").show();
        } else {
            mPresenter.changePassword(editTextOldPassword.getText().toString(), editTextNewPassword.getText().toString());
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}