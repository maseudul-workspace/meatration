package com.webinfotech.meatration.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.material.navigation.NavigationView;
import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.presentation.ui.bottomsheet.LoginBottomSheet;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @BindView(R.id.navigation)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        setContentView(R.layout.activity_base);
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        setToolbar();
        setUpNavigationView();
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void setUpNavigationView() {
        Menu navMenu = navigationView.getMenu();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_log_in:
                        goToLoginActivity();
                        break;
                    case R.id.nav_address:
                        if (isLoggedIn()) {
                            goToShippingAddressActivity();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_cart:
                        if (isLoggedIn()) {
                            goToCartActivity();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_user_profile:
                        if (isLoggedIn()) {
                            goToUserProfileActivity();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_change_password:
                        if (isLoggedIn()) {
                            goToChangePasswordActivity();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_wishlist:
                        if (isLoggedIn()) {
                            goToWishListActivity();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_orders:
                        if (isLoggedIn()) {
                            goToOrderHistory();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_about_us:
                        goToAboutActivity();
                        break;
                    case R.id.nav_log_out:
                        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
                        androidApplication.setUserInfo(getApplicationContext(), null);
                        goToMainActivity();
                        break;
                }
                return false;
            }
        });
        View headerLayout = navigationView.getHeaderView(0);
        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLoggedIn()) {
                    Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                    startActivity(intent);
                } else {
                    showLoginBottomSheet();
                }
            }
        });
        if (isLoggedIn()) {
            navMenu.findItem(R.id.nav_log_out).setVisible(true);
            navMenu.findItem(R.id.nav_log_in).setVisible(false);
        } else {
            navMenu.findItem(R.id.nav_log_out).setVisible(false);
            navMenu.findItem(R.id.nav_log_in).setVisible(true);
        }
    }

    private void goToShippingAddressActivity() {
        Intent intent = new Intent(this, ShippingAddressActivity.class);
        startActivity(intent);
    }

    private void goToCartActivity() {
        Intent intent = new Intent(this, CartListActivity.class);
        startActivity(intent);
    }

    private void goToLoginActivity() {
        Intent intent = new Intent(this, LogInActivity.class);
        startActivity(intent);
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToUserProfileActivity() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    private void goToChangePasswordActivity() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    private void goToWishListActivity() {
        Intent intent = new Intent(this, WishListActivity.class);
        startActivity(intent);
    }

    private void goToOrderHistory() {
        Intent intent = new Intent(this, OrderListActivity.class);
        startActivity(intent);
    }

    private void goToAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
            Log.e("LogMsg", "User Id: " + userInfo.userId);
            Log.e("LogMsg", "Api token: " + userInfo.apiToken);
            return true;
        } else {
            return false;
        }
    }

    private void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

}