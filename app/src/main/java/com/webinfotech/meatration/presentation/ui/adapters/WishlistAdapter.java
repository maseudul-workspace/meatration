package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.WishList;
import com.webinfotech.meatration.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.ViewHolder> {

    public interface Callback {
        void removeFromWishlist(int wishListId);
        void onProductClicked(int productId);
    }

    Context mContext;
    WishList[] wishLists;
    Callback mCallback;

    public WishlistAdapter(Context mContext, WishList[] wishLists, Callback mCallback) {
        this.mContext = mContext;
        this.wishLists = wishLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_products_vertical, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/images/products/" + wishLists[position].product.mainImage);
        holder.txtViewProductName.setText(wishLists[position].product.name);
        holder.txtViewPrice.setText("₹ " + wishLists[position].product.minPrice);
        holder.txtViewMrp.setText("₹ " + wishLists[position].product.mrp);
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(wishLists[position].product.id);
            }
        });
        if (wishLists[position].product.isWishListed) {
            holder.imgViewHeartFill.setVisibility(View.VISIBLE);
            holder.imgViewHeartOutline.setVisibility(View.GONE);
        } else {
            holder.imgViewHeartFill.setVisibility(View.GONE);
            holder.imgViewHeartOutline.setVisibility(View.VISIBLE);
        }

        holder.imgViewHeartFill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.removeFromWishlist(wishLists[position].wishListId);
            }
        });
    }

    @Override
    public int getItemCount() {
        return wishLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.img_view_heart_fill)
        ImageView imgViewHeartFill;
        @BindView(R.id.img_view_heart_outline)
        ImageView imgViewHeartOutline;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}