package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchWishListInteractor;
import com.webinfotech.meatration.domain.interactors.RemoveFromWishlistInteractor;
import com.webinfotech.meatration.domain.interactors.impl.FetchWishListInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.domain.models.WishList;
import com.webinfotech.meatration.presentation.presenters.WishListPresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.presentation.ui.adapters.WishlistAdapter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class WishListPresenterImpl extends AbstractPresenter implements WishListPresenter,
                                                                        FetchWishListInteractor.Callback,
                                                                        WishlistAdapter.Callback,
                                                                        RemoveFromWishlistInteractor.Callback
{

    Context mContext;
    WishListPresenter.View mView;

    public WishListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWishList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            FetchWishListInteractorImpl fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchWishListInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onWishListFetchSuccess(WishList[] wishLists) {
        for (int i = 0; i < wishLists.length; i++) {
            wishLists[i].product.isWishListed = true;
        }
        WishlistAdapter adapter = new WishlistAdapter(mContext, wishLists, this);
        mView.loadAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onWishListFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void removeFromWishlist(int wishListId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            RemoveFromWishlistInteractorImpl removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, wishListId);
            removeFromWishlistInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onProductClicked(int productId) {
        mView.onProductClicked(productId);
    }

    @Override
    public void onWishListRemoveSuccess() {
        fetchWishList();
    }

    @Override
    public void onWishListRemoveFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }
}
