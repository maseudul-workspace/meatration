package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.ChangePasswordInteractor;
import com.webinfotech.meatration.domain.interactors.impl.ChangePasswordInteractorImpl;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.presentation.presenters.ChangePasswordPresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ChangePasswordPresneterImpl extends AbstractPresenter implements ChangePasswordPresenter,
                                                                                ChangePasswordInteractor.Callback {

    Context mContext;
    ChangePasswordPresenter.View mView;
    ChangePasswordInteractorImpl changePasswordInteractor;

    public ChangePasswordPresneterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            changePasswordInteractor = new ChangePasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, oldPassword, newPassword);
            changePasswordInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onPasswordChangeSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Password Changed Successfully").show();
    }

    @Override
    public void onPasswordChangeFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
