package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.presentation.ui.adapters.CategoryAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.HomeViewpagerAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.ProductsHorizontalAdapter;

public interface MainPresenter {
    void fetchMainData();
    interface View {
        void loadData(HomeViewpagerAdapter adapter, CategoryAdapter categoryAdapter, int sliderLength, ProductsHorizontalAdapter newProductsAdapter, ProductsHorizontalAdapter popularProductsAdapter, int newProductsLength, int popularProductsLength);
        void onCategoryClicked(int categoryId, int isSubCategory);
        void onProductClicked(int productId);
        void showLoader();
        void hideLoader();
    }
}
