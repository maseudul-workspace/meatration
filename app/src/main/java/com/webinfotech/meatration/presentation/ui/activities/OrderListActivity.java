package com.webinfotech.meatration.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.presentation.presenters.OrderHistoryPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.OrderHistoryPresenterImpl;
import com.webinfotech.meatration.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.meatration.presentation.ui.dialogs.BankDetailsDialog;
import com.webinfotech.meatration.threading.MainThreadImpl;

public class OrderListActivity extends AppCompatActivity implements OrderHistoryPresenter.View, BankDetailsDialog.Callback {

    OrderHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    @BindView(R.id.recycler_view_orders)
    RecyclerView recyclerViewOrders;
    LinearLayoutManager layoutManager;
    BankDetailsDialog bankDetailsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        setUpBankDetailsDialog();
        mPresenter.fetchOrders(pageNo, "refresh");
        getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initialisePresenter() {
        mPresenter = new OrderHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpBankDetailsDialog() {
        bankDetailsDialog = new BankDetailsDialog(this, this, this);
        bankDetailsDialog.setUpDialog();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(OrdersAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewOrders.setLayoutManager(layoutManager);
        recyclerViewOrders.setAdapter(adapter);
        recyclerViewOrders.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();

                if(!recyclerView.canScrollVertically(1))
                {
                    if (pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchOrders(pageNo, "");
                    }
                }

            }
        });
    }

    @Override
    public void onOrderCancel() {
        isScrolling = true;
        pageNo = 1;
        totalPage = 1;
        mPresenter.fetchOrders(pageNo, "refresh");
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showBankDetailsDialog() {
        bankDetailsDialog.showDialog();
    }

    @Override
    public void onSubmitClicked(String name, String bankName, String ifscCode, String branchName, String accountNo) {
        bankDetailsDialog.hideDialog();
        mPresenter.requestRefund(name, bankName, branchName, accountNo, ifscCode);
    }

    @Override
    public void goToProductDetails(int productId, int productType) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}