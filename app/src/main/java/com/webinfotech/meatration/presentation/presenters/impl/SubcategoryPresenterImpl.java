package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchSubcategoriesInteractor;
import com.webinfotech.meatration.domain.interactors.impl.FetchSubcategoriesInteractorImpl;
import com.webinfotech.meatration.domain.models.Subcategory;
import com.webinfotech.meatration.presentation.presenters.SubcategoryPresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class SubcategoryPresenterImpl extends AbstractPresenter implements SubcategoryPresenter, FetchSubcategoriesInteractor.Callback, SubcategoryAdapter.Callback {

    Context mContext;
    SubcategoryPresenter.View mView;

    public SubcategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubcategories(int categoryId) {
        FetchSubcategoriesInteractorImpl fetchSubcategoriesInteractor = new FetchSubcategoriesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId);
        fetchSubcategoriesInteractor.execute();
    }

    @Override
    public void onGettingSubcategoriesSuccess(Subcategory[] subcategories) {
        SubcategoryAdapter subcategoryAdapter = new SubcategoryAdapter(mContext, subcategories, this);
        mView.loadData(subcategoryAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingSubcategoriesFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onSubcategoryClicked(int subcategoryId) {
        mView.onSubcategoryClicked(subcategoryId);
    }
}
