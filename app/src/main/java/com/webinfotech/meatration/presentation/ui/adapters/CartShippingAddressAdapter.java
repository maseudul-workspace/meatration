package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.ShippingAddress;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CartShippingAddressAdapter extends RecyclerView.Adapter<CartShippingAddressAdapter.ViewHolder> {

    public interface Callback {
        void onAddressSelected(int id);
        void onEditClicked(int id);
        void onDeliverButtonClicked();
    }

    Context mContext;
    ShippingAddress[] addresses;
    Callback mCallback;

    public CartShippingAddressAdapter(Context mContext, ShippingAddress[] addresses, Callback mCallback) {
        this.mContext = mContext;
        this.addresses = addresses;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart_shipping_address, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (addresses[position].isSelected) {
            holder.btnDeliver.setVisibility(View.VISIBLE);
            holder.txtViewEdit.setVisibility(View.VISIBLE);
            holder.txtViewEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onEditClicked(addresses[position].id);
                }
            });
            holder.btnDeliver.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onDeliverButtonClicked();
                }
            });
        } else {
            holder.btnDeliver.setVisibility(View.GONE);
            holder.txtViewEdit.setVisibility(View.GONE);
        }
        holder.txtViewUsername.setText(addresses[position].name);
        holder.txtViewAddress.setText(addresses[position].address + ", " + addresses[position].city + ", " + addresses[position].state + ", " + addresses[position].pin);
        holder.txtViewMobile.setText(addresses[position].mobile);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onAddressSelected(addresses[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return addresses.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_user_name)
        TextView txtViewUsername;
        @BindView(R.id.txt_view_edit)
        TextView txtViewEdit;
        @BindView(R.id.txt_view_address)
        TextView txtViewAddress;
        @BindView(R.id.txt_view_mobile)
        TextView txtViewMobile;
        @BindView(R.id.btn_deliver_here)
        Button btnDeliver;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(ShippingAddress[] addresses) {
        this.addresses = addresses;
        notifyDataSetChanged();
    }

}
