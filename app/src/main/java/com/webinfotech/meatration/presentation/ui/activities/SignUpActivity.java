package com.webinfotech.meatration.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.presentation.presenters.RegisterUserPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.RegisterUserPresenterImpl;
import com.webinfotech.meatration.threading.MainThreadImpl;

import java.util.Calendar;

public class SignUpActivity extends AppCompatActivity implements RegisterUserPresenter.View {

    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_confirm_password)
    EditText editTextConfirmPassword;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.txt_view_dob)
    TextView txtViewDOB;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    String dob;
    @BindView(R.id.radio_group_gender)
    RadioGroup radioGroupGender;
    String gender;
    RegisterUserPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        String phoneNo = getIntent().getStringExtra("phoneNo");
        editTextPhone.setText(phoneNo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign Up");
        setRadioGroupGender();
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new RegisterUserPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setRadioGroupGender() {
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb=(RadioButton)findViewById(checkedId);
                gender = rb.getText().toString();
                if (gender.equals("Male")) {
                    gender = "M";
                } else {
                    gender = "F";
                }
            }
        });
    }

    @OnClick(R.id.view_set_date) void onDOBLayoutClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewDOB.setText(dob);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextName.getText().toString().trim().isEmpty() ||
                editTextEmail.getText().toString().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty() ||
                editTextConfirmPassword.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(this, "Please fill all the fields").show();
        } else if (dob == null) {
            Toasty.warning(this, "Please enter your birthday").show();
        } else if (gender == null) {
            Toasty.warning(this, "Please enter your gender").show();
        } else if(editTextPassword.getText().toString().length() < 8) {
            Toasty.warning(this, "Password must be minumn 8 charcters in length").show();
        } else if(editTextPhone.getText().toString().length() != 10) {
            Toasty.warning(this, "Mobile must be 10 characters").show();
        } else if (!editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) {
            Toasty.warning(this, "Password mismatch").show();
        } else {
            mPresenter.registerUser(
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextPhone.getText().toString(),
                    dob,
                    gender,
                    editTextState.getText().toString(),
                    editTextCity.getText().toString(),
                    editTextPin.getText().toString(),
                    editTextAddress.getText().toString(),
                    editTextPassword.getText().toString(),
                    editTextConfirmPassword.getText().toString()
            );
            showLoader();
        }
    }

    @Override
    public void onRegisterSuccess() {
        finish();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}