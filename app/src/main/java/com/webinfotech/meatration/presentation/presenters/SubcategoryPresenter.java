package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.presentation.ui.adapters.SubcategoryAdapter;

public interface SubcategoryPresenter {
    void fetchSubcategories(int categoryId);
    interface View {
        void loadData(SubcategoryAdapter adapter);
        void showLoader();
        void hideLoader();
        void onSubcategoryClicked(int id);
    }
}
