package com.webinfotech.meatration.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.Charges;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class DeliverySpeedDialog {

    public interface Callback {
        void onProccedClicked();
        void onDeliveryOptionsSelected(int id);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.radio_group_delivery)
    RadioGroup radioGroupDelivery;
    @BindView(R.id.radio_btn_express_delivery)
    RadioButton radioBtnExpressDelivery;
    @BindView(R.id.radio_btn_normal_delivery)
    RadioButton radioBtnNormalDelivery;
    boolean isSelected = false;

    public DeliverySpeedDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.delivery_speed_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        ButterKnife.bind(this, dialogContainer);
        if (checkTimeSlot() == 2) {
            radioBtnExpressDelivery.setVisibility(View.GONE);
        }
        setRadioGroupDelivery();
    }

    private void setRadioGroupDelivery() {
        radioGroupDelivery.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId)
                {
                    case R.id.radio_btn_express_delivery:
                        mCallback.onDeliveryOptionsSelected(2);
                        break;
                    case R.id.radio_btn_normal_delivery:
                        mCallback.onDeliveryOptionsSelected(1);
                        break;
                }
                isSelected = true;
            }
        });
    }

    @OnClick(R.id.btn_proceed) void onProceedClicked() {
        if (isSelected) {
            mCallback.onProccedClicked();
        } else {
            Toasty.warning(mContext, "Please Select Delivery Timing", Toast.LENGTH_SHORT).show();
        }
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void showDialog() {
        dialog.show();
    }

    private int checkTimeSlot() {
        try {
            Date time1 = new SimpleDateFormat("HH:mm:ss").parse("07:00:00");
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            calendar1.add(Calendar.DATE, 1);


            Date time2 = new SimpleDateFormat("HH:mm:ss").parse("19:00:00");
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

            Date time5 = new SimpleDateFormat("HH:mm:ss").parse(getTime());
            Calendar calendar5 = Calendar.getInstance();
            calendar5.setTime(time5);
            calendar5.add(Calendar.DATE, 1);

            Date currentTime = calendar5.getTime();

            if (currentTime.after(calendar1.getTime()) && currentTime.before(calendar2.getTime())) {
                return 1;
            } else {
                return 2;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void loadCharges(Charges[] charges) {
        radioBtnNormalDelivery.setText("Normal Delivery - 180 min (Rs " + charges[0].amount + ")");
        radioBtnExpressDelivery.setText("Express Delivery - 90 min (Rs " + charges[1].amount + ")");
    }

    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void hideExpressDelivery() {
        radioBtnExpressDelivery.setVisibility(View.GONE);
    }

}
