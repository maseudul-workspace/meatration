package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.Category;
import com.webinfotech.meatration.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    public interface Callback {
        void onCategoryClicked(int id, int isSubCategory);
    }

    Context mContext;
    Category[] categories;
    Callback mCallback;

    public CategoryAdapter(Context mContext, Category[] categories, Callback mCallback) {
        this.mContext = mContext;
        this.categories = categories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_category, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgCategoryIcon, mContext.getResources().getString(R.string.base_url) + "/images/category/category/" + categories[position].image, 5);
        holder.txtViewCategoryName.setText(categories[position].name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCategoryClicked(categories[position].id, categories[position].isSubCategory);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_category)
        ImageView imgCategoryIcon;
        @BindView(R.id.txt_view_category_name)
        TextView txtViewCategoryName;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
