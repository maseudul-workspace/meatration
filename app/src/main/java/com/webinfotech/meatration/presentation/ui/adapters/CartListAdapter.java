package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.CartList;
import com.webinfotech.meatration.domain.models.Product;
import com.webinfotech.meatration.domain.models.Size;
import com.webinfotech.meatration.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.ViewHolder> {

    public interface Callback {
        void updateCart(int quantity, int cartId);
        void removeCart(int cartId);
    }

    Context mContext;
    CartList[] carts;
    Callback mCallback;

    public CartListAdapter(Context mContext, CartList[] carts, Callback mCallback) {
        this.mContext = mContext;
        this.carts = carts;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = carts[position].product;
        Size[] sizes = product.sizes;
        holder.txtViewProductName.setText(product.name);
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/images/products/" + product.mainImage);
        if (product.productType == 2) {
            if (carts[position].quantity > 6) {
                for (int i = 0; i < sizes.length; i++) {
                    if (sizes[i].size == 6) {
                        holder.txtViewPrice.setText("Rs. " + sizes[i].price * carts[position].quantity);
                        holder.txtViewMrp.setText("Rs. " + sizes[i].mrp * carts[position].quantity);
                        break;
                    }
                }
            } else {
                for (int i = 0; i < sizes.length; i++) {
                    if (sizes[i].size == carts[position].quantity) {
                        holder.txtViewPrice.setText("Rs. " + sizes[i].price * carts[position].quantity);
                        holder.txtViewMrp.setText("Rs. " + sizes[i].mrp * carts[position].quantity);
                        break;
                    }
                }
            }
            holder.txtViewSize.setText(carts[position].quantity + " kg");
        } else {
            for (int i = 0; i < sizes.length; i++) {
                if (sizes[i].id == carts[position].sizeId) {
                    holder.txtViewPrice.setText("Rs. " + sizes[i].price);
                    holder.txtViewMrp.setText("Rs. " + sizes[i].mrp);
                    if (sizes[i].sizeTypeId == 1) {
                        holder.txtViewSize.setText(sizes[i].size + " kg");
                    } else {
                        holder.txtViewSize.setText(sizes[i].size + " lt");
                    }
                    break;
                }
            }
            holder.layoutQtyPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int stock = 0;
                    for (int i = 0; i < sizes.length; i++) {
                        if (sizes[i].id == carts[position].sizeId) {
                            stock = sizes[i].stock;
                            break;
                        }
                    }
                    if (carts[position].quantity < stock)
                        carts[position].quantity = carts[position].quantity + 1;
                    holder.txtViewQty.setText(Integer.toString(carts[position].quantity));
                }
            });
        }

        holder.txtViewQty.setText(Integer.toString(carts[position].quantity));
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        holder.layoutQtyMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (carts[position].quantity > 1) {
                    carts[position].quantity = carts[position].quantity - 1;
                    holder.txtViewQty.setText(Integer.toString(carts[position].quantity));
                }
            }
        });

        holder.btnUpdateCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.updateCart(carts[position].quantity, carts[position].cartId);
            }
        });

        holder.btnRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation Message");
                builder.setMessage("You are about to remove a item from cart. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#00BFA5'>Yes</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.removeCart(carts[position].cartId);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.show();
            }
        });

        if (carts[position].quantity > 6) {
            holder.txtViewBulkOrdersWarning.setVisibility(View.VISIBLE);
        } else {
            holder.txtViewBulkOrdersWarning.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return carts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQty;
        @BindView(R.id.txt_view_size)
        TextView txtViewSize;
        @BindView(R.id.layout_qty_minus)
        View layoutQtyMinus;
        @BindView(R.id.layout_qty_plus)
        View layoutQtyPlus;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.btn_update_cart)
        Button btnUpdateCart;
        @BindView(R.id.btn_remove_item)
        Button btnRemoveItem;
        @BindView(R.id.txt_view_bulk_orders_warning)
        TextView txtViewBulkOrdersWarning;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
