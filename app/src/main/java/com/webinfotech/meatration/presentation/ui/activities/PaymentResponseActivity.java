package com.webinfotech.meatration.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.webinfotech.meatration.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PaymentResponseActivity extends AppCompatActivity {

    @BindView(R.id.layout_success)
    View layoutSuccess;
    @BindView(R.id.layout_failed)
    View layoutFailed;
    @BindView(R.id.txt_view_order_id)
    TextView txtViewOrderId;
    @BindView(R.id.txt_view_failed_order_id)
    TextView txtViewFailedOrderId;
    @BindView(R.id.txt_view_order_date)
    TextView txtViewOrderDate;
    @BindView(R.id.txt_view_failed_order_date)
    TextView txtViewFailedOrderDate;
    @BindView(R.id.txt_view_failed_payment_type)
    TextView txtViewFailedPaymentType;
    @BindView(R.id.txt_view_payment_id)
    TextView txtViewPaymentId;
    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;
    @BindView(R.id.txt_view_failed_amount)
    TextView txtViewFailedAmount;
    @BindView(R.id.txt_view_error_msg)
    TextView txtViewErrorMsg;
    String orderId;
    int orderStatus;
    String transactionId;
    String amount;
    String paymentId;
    String errorMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_response);
        ButterKnife.bind(this);
        orderId = getIntent().getStringExtra("orderId");
        paymentId = getIntent().getStringExtra("paymentId");
        orderStatus = getIntent().getIntExtra("orderStatus", 0);
        transactionId = getIntent().getStringExtra("transactionId");
        amount = getIntent().getStringExtra("amount");
        errorMsg = getIntent().getStringExtra("errorMsg");
        setUpUI();
    }

    public void setUpUI() {
        if (orderStatus == 1) {
            layoutSuccess.setVisibility(View.VISIBLE);
            txtViewOrderId.setText(orderId);
            txtViewOrderDate.setText(getDate());
            txtViewAmount.setText("Rs. " + amount);
            txtViewPaymentId.setText(paymentId);
        } else {
            layoutFailed.setVisibility(View.VISIBLE);
            txtViewFailedOrderId.setText(orderId);
            txtViewFailedOrderDate.setText(getDate());
            txtViewFailedAmount.setText("Rs. " + amount);
            txtViewErrorMsg.setText(errorMsg);
        }
    }

    public static String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MMMM dd, yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @OnClick(R.id.btn_order_history) void onOrderHistoryClicked() {
        Intent intent = new Intent(this, OrderListActivity.class);
        startActivity(intent);
    }

}