package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.domain.models.UserInfo;

public interface UserProfilePresenter {
    void fetchUserProfile();
    void updateProfile(String name,
                       String email,
                       String mobile,
                       String DOB,
                       String gender,
                       String city,
                       String state,
                       String pin,
                       String address);
    interface View {
        void showLoader();
        void hideLoader();
        void loadUserProfileData(UserInfo userInfo);
    }
}
