package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.CancelOrderInteractor;
import com.webinfotech.meatration.domain.interactors.FetchOrderHistoryInteractor;
import com.webinfotech.meatration.domain.interactors.RequestRefundInteractor;
import com.webinfotech.meatration.domain.interactors.impl.CancelOrderInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.FetchOrderHistoryInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.RequestRefundInteractorImpl;
import com.webinfotech.meatration.domain.models.Order;
import com.webinfotech.meatration.domain.models.Product;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.presentation.presenters.OrderHistoryPresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.ProductsVerticalAdapter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class OrderHistoryPresenterImpl extends AbstractPresenter implements OrderHistoryPresenter,
                                                                            FetchOrderHistoryInteractor.Callback,
                                                                            OrdersAdapter.Callback,
                                                                            CancelOrderInteractor.Callback,
                                                                            RequestRefundInteractor.Callback
{
    Context mContext;
    OrderHistoryPresenter.View mView;
    Order[] newOrders;
    OrdersAdapter adapter;
    int orderId;

    public OrderHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchOrders(int pageNo, String refresh) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            if (refresh.equals("refresh")) {
                newOrders = null;
            }
            FetchOrderHistoryInteractorImpl fetchOrderHistoryInteractor = new FetchOrderHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, pageNo);
            fetchOrderHistoryInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void requestRefund(String name, String bankName, String branchName, String accountName, String ifsc) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            RequestRefundInteractorImpl requestRefundInteractor = new RequestRefundInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, orderId, name, bankName, branchName, accountName, ifsc);
            requestRefundInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingOrderHistorySuccess(Order[] orders, int totalPage) {
        Order[] tempOrders;
        tempOrders = newOrders;
        try {
            int len1 = tempOrders.length;
            int len2 = orders.length;
            newOrders = new Order[len1 + len2];
            System.arraycopy(tempOrders, 0, newOrders, 0, len1);
            System.arraycopy(orders, 0, newOrders, len1, len2);
            adapter.updateData(newOrders);
            adapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            newOrders = orders;
            adapter = new OrdersAdapter(mContext, orders, this);
            mView.loadAdapter(adapter, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderHistoryFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onCancelClicked(int orderId, int type) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            if (type == 1) {
                CancelOrderInteractorImpl cancelOrderInteractor = new CancelOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, orderId);
                cancelOrderInteractor.execute();
                mView.showLoader();
            } else {
                this.orderId = orderId;
                mView.showBankDetailsDialog();
            }
        }
    }

    @Override
    public void onProductClicked(int productId, int type) {
        mView.goToProductDetails(productId, type);
    }

    @Override
    public void onOrderCancelSuccess() {
        mView.onOrderCancel();
    }

    @Override
    public void onOrderCancelFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onRequestRefundSuccess() {
        mView.onOrderCancel();
    }

    @Override
    public void onRequestRefundFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

}
