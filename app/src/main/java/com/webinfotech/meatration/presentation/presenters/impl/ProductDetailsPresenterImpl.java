package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.AddToCartIntercator;
import com.webinfotech.meatration.domain.interactors.FetchProductDetailsInteractor;
import com.webinfotech.meatration.domain.interactors.impl.AddToCartInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.FetchCartListInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.FetchOrderHistoryInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.FetchProductDetailsInteractorImpl;
import com.webinfotech.meatration.domain.models.CartList;
import com.webinfotech.meatration.domain.models.Product;
import com.webinfotech.meatration.domain.models.ProductDetailsData;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.presentation.presenters.ProductDetailsPresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.presentation.ui.adapters.RelatedProductsAdapter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class ProductDetailsPresenterImpl extends AbstractPresenter implements   ProductDetailsPresenter,
                                                                                FetchProductDetailsInteractor.Callback,
                                                                                AddToCartIntercator.Callback,
                                                                                FetchCartListInteractorImpl.Callback,
                                                                                RelatedProductsAdapter.Callback

{

    Context mContext;
    ProductDetailsPresenter.View mView;

    public ProductDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductDetails(int productId) {
        FetchProductDetailsInteractorImpl fetchProductDetailsInteractor = new FetchProductDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, productId);
        fetchProductDetailsInteractor.execute();
    }

    @Override
    public void fetchCartDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchCartListInteractorImpl fetchCartListInteractor = new FetchCartListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchCartListInteractor.execute();
        }
    }

    @Override
    public void addToCart(int productId, String qty, String sizeId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            AddToCartInteractorImpl addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, productId, qty, sizeId);
            addToCartInteractor.execute();
            mView.showLoader();
        } else {
            mView.showLoginSnackbar();
        }
    }

    @Override
    public void onGettingProductDetailsSuccess(ProductDetailsData productDetailsData) {
        Product[] choosedProducts = productDetailsData.relatedData.choosedProducts;
        Product[] relatedProducts = productDetailsData.relatedData.relatedProducts;
        ArrayList<Product> relatedProductsList = new ArrayList<>();

        for (int i = 0; i < choosedProducts.length; i++) {
            relatedProductsList.add(choosedProducts[i]);
        }

        for (int i = 0; i < relatedProducts.length; i++) {
            relatedProductsList.add(relatedProducts[i]);
        }

        RelatedProductsAdapter adapter = new RelatedProductsAdapter(mContext, relatedProductsList, this);

        mView.loadData(productDetailsData.product, adapter);

        mView.hideLoader();
    }

    @Override
    public void onGettingProductDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onAddToCartSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Successfully Added To Cart").show();
        fetchCartDetails();
    }

    @Override
    public void onAddToCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onGettingCartListSuccess(CartList[] cartLists) {
        mView.loadCartCount(cartLists.length);
    }

    @Override
    public void onGettingCartListFail(String errorMsg, int loginError) {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.onProductClicked(productId);
    }

}
