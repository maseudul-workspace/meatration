package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.Order;
import com.webinfotech.meatration.util.GlideHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> implements OrderProductsAdapter.Callback {

    @Override
    public void onProductClicked(int productId, int type) {
        mCallback.onProductClicked(productId, type);
    }

    public interface Callback {
        void onCancelClicked(int orderId, int type);
        void onProductClicked(int productId, int type);
    }

    Context mContext;
    Order[] orders;
    Callback mCallback;

    public OrdersAdapter(Context mContext, Order[] orders, Callback mCallback) {
        this.mContext = mContext;
        this.orders = orders;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_orders, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtViewOrderNo.setText("ORDER NO " + orders[position].orderId);
        holder.txtViewTotalAmount.setText("Rs. " + orders[position].totalAmount);
        holder.txtViewDeliveryCharges.setText("Rs. " + orders[position].shippingCharges);

        switch (orders[position].deliveryStatus) {
            case 1:
                holder.txtViewStatus.setText("Processing");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_A400));
                break;
            case 2:
                holder.txtViewStatus.setText("Accepted");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A400));
                break;
            case 3:
                holder.txtViewStatus.setText("On The Way");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_A400));
                break;
            case 4:
                holder.txtViewStatus.setText("Delivered");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A400));
                break;
            case 5:
                holder.txtViewStatus.setText("Cancelled");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_red_A400));
                break;
        }

        if (orders[position].paymentStatus == 1) {
            holder.txtViewPaymentStatus.setText("COD");
        } else if (orders[position].paymentStatus == 2) {
            holder.txtViewPaymentStatus.setText("Paid");
        } else {
            holder.txtViewPaymentStatus.setText("Failed");
        }

        if (orders[position].paymentType == 1) {
            holder.txtViewPaymentMethod.setText("COD");
        } else {
            holder.txtViewPaymentMethod.setText("Online");
        }

        if (orders[position].deliveryStatus == 1 || orders[position].deliveryStatus == 2) {
            holder.layoutCancel.setVisibility(View.VISIBLE);
            holder.layoutCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Confirmation dialog");
                    builder.setMessage("You are about to cancel a order. Do you really want to proceed ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton(Html.fromHtml("<font color='#00BFA5'>Yes</font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCallback.onCancelClicked(orders[position].orderId, orders[position].paymentType);
                        }
                    });

                    builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'>No</font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();
                }
            });
        } else {
            holder.layoutCancel.setVisibility(View.GONE);
        }

        OrderProductsAdapter adapter = new OrderProductsAdapter(mContext, orders[position].orderProducts, this);
        holder.recyclerViewOrderProducts.setAdapter(adapter);
        holder.recyclerViewOrderProducts.setLayoutManager(new LinearLayoutManager(mContext));
        holder.recyclerViewOrderProducts.addItemDecoration(new DividerItemDecoration(mContext, LinearLayout.VERTICAL));
        holder.txtViewOrderDate.setText("Placed On: " + changeDataFormat(orders[position].date));
    }

    @Override
    public int getItemCount() {
        return orders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_order_no)
        TextView txtViewOrderNo;
        @BindView(R.id.txt_view_order_date)
        TextView txtViewOrderDate;
        @BindView(R.id.txt_view_total_amount)
        TextView txtViewTotalAmount;
        @BindView(R.id.txt_view_delivery_charges)
        TextView txtViewDeliveryCharges;
        @BindView(R.id.layout_cancel)
        View layoutCancel;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.txt_view_payment_method)
        TextView txtViewPaymentMethod;
        @BindView(R.id.txt_view_payment_status)
        TextView txtViewPaymentStatus;
        @BindView(R.id.recycler_view_order_products)
        RecyclerView recyclerViewOrderProducts;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(Order[] orders) {
        this.orders = orders;
        notifyDataSetChanged();
    }

    private String changeDataFormat(String dateString) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "EEEE, dd MMM, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateString);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
