package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchHomeDataInteractor;
import com.webinfotech.meatration.domain.interactors.impl.FetchHomeDataInteractorImpl;
import com.webinfotech.meatration.domain.models.HomeData;
import com.webinfotech.meatration.presentation.presenters.MainPresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.presentation.ui.adapters.CategoryAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.HomeViewpagerAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.ProductsHorizontalAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.ProductsVerticalAdapter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    FetchHomeDataInteractor.Callback,
                                                                    CategoryAdapter.Callback,
                                                                    ProductsHorizontalAdapter.Callback
{

    Context mContext;
    MainPresenter.View mView;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMainData() {
        FetchHomeDataInteractorImpl fetchHomeDataInteractor = new FetchHomeDataInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchHomeDataInteractor.execute();
    }

    @Override
    public void onGettingHomeDataSuccess(HomeData homeData) {
        CategoryAdapter categoryAdapter = new CategoryAdapter(mContext, homeData.categories, this);
        HomeViewpagerAdapter homeViewpagerAdapter = new HomeViewpagerAdapter(mContext, homeData.sliders);
        ProductsHorizontalAdapter newProductsAdapter = new ProductsHorizontalAdapter(mContext, homeData.newProducts, this);
        ProductsHorizontalAdapter popularProductsAdapter = new ProductsHorizontalAdapter(mContext, homeData.popularProducts, this);
        mView.loadData(homeViewpagerAdapter, categoryAdapter, homeData.sliders.length, newProductsAdapter, popularProductsAdapter, homeData.newProducts.length, homeData.popularProducts.length);
    }

    @Override
    public void onGettingHomeDataFail(String errorMsg) {

    }

    @Override
    public void onCategoryClicked(int id, int isSubCategory) {
        mView.onCategoryClicked(id, isSubCategory);
    }

    @Override
    public void onProductClicked(int productId) {
        Log.e("LogMsg", "On Product Clicked");
        mView.onProductClicked(productId);
    }
}
