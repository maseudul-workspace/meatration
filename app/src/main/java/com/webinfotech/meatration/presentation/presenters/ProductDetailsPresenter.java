package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.domain.models.Product;
import com.webinfotech.meatration.presentation.ui.adapters.RelatedProductsAdapter;

public interface ProductDetailsPresenter {
    void fetchProductDetails(int productId);
    void addToCart(int productId, String qty, String sizeId);
    void fetchCartDetails();
    interface View {
        void loadData(Product product, RelatedProductsAdapter relatedProductsAdapter);
        void loadCartCount(int cartCount);
        void onProductClicked(int productId);
        void showLoginSnackbar();
        void showLoader();
        void hideLoader();
    }
}
