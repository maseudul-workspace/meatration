package com.webinfotech.meatration.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.presentation.presenters.PhoneNumberVerificationPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.PhoneNumberVerificationPresenterImpl;
import com.webinfotech.meatration.threading.MainThreadImpl;

public class PhoneNumberVerificationActivity extends AppCompatActivity implements PhoneNumberVerificationPresenter.View {

    @BindView(R.id.phone_number_layout)
    View phoneNumberLayout;
    @BindView(R.id.otp_layout)
    View otpLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.pinview)
    Pinview pinview;
    ProgressDialog progressDialog;
    PhoneNumberVerificationPresenterImpl mPresenter;
    String otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number_verification);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Check OTP");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new PhoneNumberVerificationPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    @OnClick(R.id.btn_otp) void sendOTP() {
        txtInputPhoneLayout.setError("");
        if (editTextPhone.getText().toString().isEmpty()) {
            txtInputPhoneLayout.setError("Phone No Required");
        } else if (editTextPhone.getText().toString().trim().length() < 10) {
            txtInputPhoneLayout.setError("Phone No Must Be 10 Characters");
        } else {
            mPresenter.sendOtp(editTextPhone.getText().toString());
            showLoader();
        }
    }

    @OnClick(R.id.btn_check_otp) void onCheckOtpClicked() {
        if (pinview.getValue().length() < 6) {
            Toasty.warning(this, "OTP should be 6 digit", Toast.LENGTH_SHORT).show();
        } else {
            if (pinview.getValue().toString().equals(otp)) {
                Intent intent = new Intent(this, SignUpActivity.class);
                intent.putExtra("phoneNo", editTextPhone.getText().toString());
                startActivity(intent);
                finish();
            } else {
                Toasty.warning(this, "Wrong Otp", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onSendOtpSuccess(String otp) {
        otpLayout.setVisibility(View.VISIBLE);
        phoneNumberLayout.setVisibility(View.GONE);
        this.otp = otp;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}