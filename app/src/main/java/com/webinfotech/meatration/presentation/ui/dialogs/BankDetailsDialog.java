package com.webinfotech.meatration.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import com.webinfotech.meatration.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class BankDetailsDialog {

    public interface Callback {
        void onSubmitClicked(String name, String bankName, String ifscCode, String branchName, String accountNo);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_bank_name)
    EditText editTextBankName;
    @BindView(R.id.edit_text_ifsc_code)
    EditText editTextIfscCode;
    @BindView(R.id.edit_text_branch_name)
    EditText editTextBranchName;
    @BindView(R.id.edit_text_account_no)
    EditText editTextAccountNo;
    @BindView(R.id.edit_text_repeat_account_no)
    EditText editTextRepeatAccountNo;

    public BankDetailsDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = callback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_bank_details_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        hideDialog();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (    editTextName.getText().toString().trim().isEmpty() ||
                editTextAccountNo.getText().toString().trim().isEmpty() ||
                editTextBankName.getText().toString().trim().isEmpty() ||
                editTextBranchName.getText().toString().trim().isEmpty() ||
                editTextIfscCode.getText().toString().trim().isEmpty() ||
                editTextRepeatAccountNo.getText().toString().isEmpty()
        ) {
            Toasty.warning(mContext, "All Fields Are Required").show();
        } else if (!editTextAccountNo.getText().toString().trim().equals(editTextRepeatAccountNo.getText().toString().trim())) {
            Toasty.warning(mContext, "Account Numbers Mismatched").show();
        } else {
            mCallback.onSubmitClicked(
                    editTextName.getText().toString(),
                    editTextBankName.getText().toString(),
                    editTextIfscCode.getText().toString(),
                    editTextBranchName.getText().toString(),
                    editTextAccountNo.getText().toString()
            );
        }
    }
}
