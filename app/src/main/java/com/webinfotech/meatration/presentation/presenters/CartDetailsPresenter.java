package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.domain.models.Charges;
import com.webinfotech.meatration.presentation.ui.adapters.CartListAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.CartShippingAddressAdapter;

public interface CartDetailsPresenter {
    void fetchCartDetails();
    void fetchShippingAddressList();
    void fetchChargesList();
    interface View {
        void showLoader();
        void hideLoader();
        void loadCartAdapter(CartListAdapter adapter, double totalAmount, double discount, double subTotal);
        void loadShippingAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter);
        void onShippingAddressEditClicked(int addressId);
        void onAddressSelected(int addressId);
        void goToPaymentActivity();
        void loadCharges(Charges[] charges);
        void showErrorMessage(String errorMsg);
        void hideViews();
        void hideExpressDeliveryOption();
    }
}
