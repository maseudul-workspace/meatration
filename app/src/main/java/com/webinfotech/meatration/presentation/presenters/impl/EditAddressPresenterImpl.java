package com.webinfotech.meatration.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchShippingAddressDetailsInteractor;
import com.webinfotech.meatration.domain.interactors.UpdateAddressInteractor;
import com.webinfotech.meatration.domain.interactors.impl.FetchShippingAddressDetailsInteractorImpl;
import com.webinfotech.meatration.domain.interactors.impl.UpdateAddressInteractorImpl;
import com.webinfotech.meatration.domain.models.ShippingAddress;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.presentation.presenters.EditAddressPresenter;
import com.webinfotech.meatration.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class EditAddressPresenterImpl extends AbstractPresenter implements EditAddressPresenter,
                                                                            FetchShippingAddressDetailsInteractor.Callback,
                                                                            UpdateAddressInteractor.Callback
{

    Context mContext;
    EditAddressPresenter.View mView;
    FetchShippingAddressDetailsInteractorImpl fetchShippingAddressDetailsInteractor;
    UpdateAddressInteractorImpl updateAddressInteractorImpl;

    public EditAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchAddressDetails(int addressId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchShippingAddressDetailsInteractor = new FetchShippingAddressDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, addressId);
            fetchShippingAddressDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void updateAddress(int addressId, String name, String email, String mobile, String city, String state, String pin, String address,  double latitude, double longitude) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            updateAddressInteractorImpl = new UpdateAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken, addressId, name, email, mobile, state, city, pin, address,  latitude, longitude);
            updateAddressInteractorImpl.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingAddressDetailsSuccess(ShippingAddress shippingAddress) {
        mView.loadAddressDetails(shippingAddress);
        mView.hideLoader();
    }

    @Override
    public void onGettingAddressDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onUpdateAddressSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Address Updated Successfully").show();
    }

    @Override
    public void onUpdateAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
