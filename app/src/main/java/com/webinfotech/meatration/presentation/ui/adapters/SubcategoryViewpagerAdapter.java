package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.testing.SubcategoryViewpager;
import com.webinfotech.meatration.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubcategoryViewpagerAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<SubcategoryViewpager> subcategoryViewpagers;
    @BindView(R.id.txt_view_category_name)
    TextView txtViewCategoryName;
    @BindView(R.id.txt_view_category_desc)
    TextView txtViewCategoryDesc;
    @BindView(R.id.img_view_category)
    ImageView imgViewCategory;

    public SubcategoryViewpagerAdapter(Context mContext, ArrayList<SubcategoryViewpager> subcategoryViewpagers) {
        this.mContext = mContext;
        this.subcategoryViewpagers = subcategoryViewpagers;
    }

    @Override
    public int getCount() {
        return subcategoryViewpagers.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Log.e("LogMsg", "Position: " + position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.subcategory_viewpager_layout, container, false);
        ButterKnife.bind(this, layout);
        container.addView(layout);
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }, 500);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public void setViews(int position) {
        imgViewCategory.setVisibility(View.VISIBLE);
        GlideHelper.setImageView(mContext, imgViewCategory, subcategoryViewpagers.get(position).image);
        Animation rightToLeft1 = AnimationUtils.loadAnimation(mContext, R.anim.right_to_left);
        Animation rightToLeft2 = AnimationUtils.loadAnimation(mContext, R.anim.right_to_left);
        Animation leftToRight = AnimationUtils.loadAnimation(mContext, R.anim.left_to_right);
        leftToRight.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Log.e("LogMsg", "Image Animation Start");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                txtViewCategoryName.setVisibility(View.VISIBLE);
                txtViewCategoryName.setAnimation(rightToLeft1);
                Log.e("LogMsg", "Image Animation Ended");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        rightToLeft1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Log.e("LogMsg", "Text Animation Start");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                txtViewCategoryDesc.setVisibility(View.VISIBLE);
                txtViewCategoryDesc.setAnimation(rightToLeft2);
                Log.e("LogMsg", "Text Animation Ended");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imgViewCategory.setAnimation(leftToRight);
    }

}
