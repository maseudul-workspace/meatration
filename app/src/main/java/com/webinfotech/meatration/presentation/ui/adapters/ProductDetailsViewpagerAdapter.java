package com.webinfotech.meatration.presentation.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.models.Image;
import com.webinfotech.meatration.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class ProductDetailsViewpagerAdapter extends PagerAdapter {

    public interface Callback {
        void onImageClicked(int id);
    }

    Context mContext;
    Image[] images;
    Callback mCallback;

    public ProductDetailsViewpagerAdapter(Context mContext, Image[] images, Callback mCallback) {
        this.mContext = mContext;
        this.images = images;
        this.mCallback = mCallback;
    }


    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        GlideHelper.setImageView(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "/images/products/" + images[position].image);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
