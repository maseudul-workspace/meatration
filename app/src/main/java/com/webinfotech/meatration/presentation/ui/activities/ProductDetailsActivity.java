package com.webinfotech.meatration.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.text.HtmlCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.webinfotech.meatration.AndroidApplication;
import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.domain.models.CartList;
import com.webinfotech.meatration.domain.models.Product;
import com.webinfotech.meatration.domain.models.Size;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.domain.models.testing.Image;
import com.webinfotech.meatration.presentation.presenters.ProductDetailsPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.ProductDetailsPresenterImpl;
import com.webinfotech.meatration.presentation.ui.adapters.ProductDetailsViewpagerAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.RelatedProductsAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.SizesAdapter;
import com.webinfotech.meatration.presentation.ui.bottomsheet.LoginBottomSheet;
import com.webinfotech.meatration.threading.MainThreadImpl;
import com.webinfotech.meatration.util.GlideHelper;

import java.util.ArrayList;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsPresenter.View, SizesAdapter.Callback, ProductDetailsViewpagerAdapter.Callback {

    @BindView(R.id.recycler_view_sizes)
    RecyclerView recyclerViewSizes;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.add_to_cart_layout)
    View addToCartLayout;
    @BindView(R.id.viewpager_dots_layout)
    LinearLayout dotsIndicatorLayout;
    @BindView(R.id.txt_view_product_name)
    TextView txtViewProductName;
    @BindView(R.id.txt_view_price)
    TextView txtViewProductPrice;
    @BindView(R.id.txt_view_mrp)
    TextView txtViewMrp;
    @BindView(R.id.txt_view_description)
    TextView txtViewDescription;
    @BindView(R.id.recycler_view_related_products)
    RecyclerView recyclerViewRelatedProducts;
    ProductDetailsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int productId;
    SizesAdapter sizesAdapter;
    Size[] sizes;
    int minQty;
    int quantity = 1;
    @BindView(R.id.txt_view_qty)
    TextView txtViewQty;
    @BindView(R.id.layout_check_meat_price)
    View layoutCheckMeatPrice;
    @BindView(R.id.layout_size)
    View layoutSize;
    @BindView(R.id.main_layout)
    View mainLayout;
    String sizeId = null;
    @BindView(R.id.size_more)
    View sizeMore;
    @BindView(R.id.qty_change_layout)
    View qtyChangeLayout;
    @BindView(R.id.grey_view)
    View greyView;
    @BindView(R.id.accent_view)
    View accentView;
    @BindView(R.id.edit_text_qty)
    EditText editTextQty;
    int productType;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_view_cart_count)
    TextView txtViewCartCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        productId = getIntent().getIntExtra("productId", 0);
        Log.e("LogMsg", "Product Id: " + productId);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Html.fromHtml("<b>Product Details</b>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchProductDetails(productId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void prepareViewpager1DotsIndicator(int sliderPosition, int length) {
        if(dotsIndicatorLayout.getChildCount() > 0){
            dotsIndicatorLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            dotsIndicatorLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void loadData(Product product, RelatedProductsAdapter relatedProductsAdapter) {
        txtViewProductName.setText(product.name);
        txtViewDescription.setText(product.description);
        if (product.sizes[0].sizeTypeId == 1) {
            txtViewProductPrice.setText("₹ " + product.minPrice + "/kg");
            txtViewMrp.setText("₹ " + product.mrp + "/kg");
        } else {
            txtViewProductPrice.setText("₹ " + product.minPrice + "/lt");
            txtViewMrp.setText("₹ " + product.mrp + "/lt");
        }
        txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        txtViewDescription.setText(HtmlCompat.fromHtml(String.valueOf(HtmlCompat.fromHtml(product.description, HtmlCompat.FROM_HTML_MODE_COMPACT)), HtmlCompat.FROM_HTML_MODE_COMPACT));
        productType = product.productType;
        if (product.productType == 1) {
            layoutSize.setVisibility(View.VISIBLE);
        } else {
            sizeMore.setVisibility(View.VISIBLE);
            qtyChangeLayout.setVisibility(View.GONE);
        }

        product.sizes[0].isSelected = true;

        this.sizes = product.sizes;

        minQty = this.sizes[0].stock;

        sizeId = Integer.toString(this.sizes[0].id);

        sizesAdapter = new SizesAdapter(this, product.sizes, this);

        ProductDetailsViewpagerAdapter adapter = new ProductDetailsViewpagerAdapter(this, product.images, this);

        viewPager.setAdapter(adapter);

        recyclerViewSizes.setAdapter(sizesAdapter);
        recyclerViewSizes.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareViewpager1DotsIndicator(position, product.images.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        prepareViewpager1DotsIndicator(0, product.images.length);

        recyclerViewRelatedProducts.setAdapter(relatedProductsAdapter);
        recyclerViewRelatedProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

    }

    @Override
    public void loadCartCount(int cartCount) {
        if (cartCount > 0) {
            txtViewCartCount.setText(Integer.toString(cartCount));
            txtViewCartCount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onProductClicked(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onSizeSelect(int position) {
        minQty = sizes[position].stock;
        quantity = 1;
        txtViewQty.setText(Integer.toString(quantity));
        if (sizes[position].sizeTypeId == 1) {
            txtViewProductPrice.setText("₹ " + sizes[position].price + "/kg");
            txtViewMrp.setText("₹ " + sizes[position].mrp + "/kg");
        } else {
            txtViewProductPrice.setText("₹ " + sizes[position].price + "/lt");
            txtViewMrp.setText("₹ " + sizes[position].mrp + "/lt");
        }
        for (int i = 0; i < sizes.length; i++) {
            if (i == position) {
                sizes[i].isSelected = true;
            } else {
                sizes[i].isSelected = false;
            }
        }
        sizesAdapter.updateData(sizes);
        if (productType == 1) {
            sizeId = Integer.toString(sizes[position].id);
        } else {
            quantity = sizes[position].size;
        }
        greyView.setVisibility(View.VISIBLE);
        accentView.setVisibility(View.GONE);
        layoutCheckMeatPrice.setVisibility(View.GONE);
        addToCartLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.grey_view) void onSizeGreyViewClicked() {
        for (int i = 0; i < sizes.length; i++) {
            sizes[i].isSelected = false;
        }
        sizesAdapter.updateData(sizes);
        greyView.setVisibility(View.GONE);
        accentView.setVisibility(View.VISIBLE);
        layoutCheckMeatPrice.setVisibility(View.VISIBLE);
        addToCartLayout.setVisibility(View.GONE);
        for (int i = 0; i < sizes.length; i++) {
            if (sizes[i].size == 6) {
                txtViewProductPrice.setText("₹ " + sizes[i].price + "/kg");
                txtViewMrp.setText("₹ " + sizes[i].mrp + "/kg");
                break;
            }
        }
        sizeId = null;
    }

    @OnClick(R.id.txt_view_add_qty) void onAddQtyClicked() {
        if (quantity < minQty) {
            quantity ++;
            txtViewQty.setText(Integer.toString(quantity));
        } else {
            Toasty.normal(this, "Item Limit Exceeded").show();
        }
    }

    @OnClick(R.id.txt_view_minus_qty) void onMinusQtyClicked() {
        if (quantity > 1) {
            quantity --;
            txtViewQty.setText(Integer.toString(quantity));
        } else {
            Toasty.normal(this, "Cant Go Below").show();
        }
    }

    @OnClick(R.id.btn_add_to_cart) void onAddToCartClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo == null) {
            showLoginBottomSheet();
        } else {
            mPresenter.addToCart(productId, Integer.toString(quantity), sizeId);
        }
    }

    @OnClick(R.id.btn_meat_add_to_cart) void onMeatAddToCart() {
        mPresenter.addToCart(productId, editTextQty.getText().toString(), sizeId);
    }

    @Override
    public void onImageClicked(int id) {

    }

    @Override
    public void showLoginSnackbar() {
        Snackbar snackbar = Snackbar.make(mainLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(loginIntent);
            }
        });
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartDetails();
    }

    @OnClick(R.id.layout_cart) void onCartClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo == null) {
            showLoginBottomSheet();
        } else {
            goToCartListActivity();
        }
    }

    private void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    private void goToCartListActivity() {
        Intent intent = new Intent(this, CartListActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}