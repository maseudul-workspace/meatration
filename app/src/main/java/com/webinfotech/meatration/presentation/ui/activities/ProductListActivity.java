package com.webinfotech.meatration.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.AbsListView;

import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.domain.models.testing.Product;
import com.webinfotech.meatration.presentation.presenters.ProductListPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.ProductListPresenterImpl;
import com.webinfotech.meatration.presentation.ui.adapters.ProductsVerticalAdapter;
import com.webinfotech.meatration.presentation.ui.bottomsheet.LoginBottomSheet;
import com.webinfotech.meatration.threading.MainThreadImpl;

import java.util.ArrayList;

public class ProductListActivity extends AppCompatActivity implements ProductListPresenter.View {

    @BindView(R.id.recycler_view_product_list)
    RecyclerView recyclerViewProductList;
    LinearLayoutManager layoutManager;
    ProductListPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    int categoryId;
    int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        getSupportActionBar().setTitle(Html.fromHtml("<b>Product List</b>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        categoryId = getIntent().getIntExtra("categoryId", 0);
        type = getIntent().getIntExtra("type", 0);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchWishList();
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ProductListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(ProductsVerticalAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new GridLayoutManager(this,2);
        recyclerViewProductList.setLayoutManager(layoutManager);
        recyclerViewProductList.setAdapter(adapter);
        recyclerViewProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchProductList(categoryId, type, pageNo);
                    }
                }
            }
        });
    }

    @Override
    public void onProductClicked(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void fetchProductList() {
        mPresenter.fetchProductList(categoryId, type, pageNo);
    }

    @Override
    public void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}