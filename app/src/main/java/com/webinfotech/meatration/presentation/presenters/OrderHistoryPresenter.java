package com.webinfotech.meatration.presentation.presenters;

import com.webinfotech.meatration.presentation.ui.adapters.OrdersAdapter;

public interface OrderHistoryPresenter {
    void fetchOrders(int pageNo, String refresh);
    void requestRefund(String name,
                       String bankName,
                       String branchName,
                       String accountName,
                       String ifsc);
    interface View {
        void loadAdapter(OrdersAdapter adapter, int totalPage);
        void showBankDetailsDialog();
        void onOrderCancel();
        void goToProductDetails(int productId, int productType);
        void showLoader();
        void hideLoader();
    }
}
