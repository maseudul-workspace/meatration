package com.webinfotech.meatration.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.webinfotech.meatration.R;
import com.webinfotech.meatration.domain.executors.impl.ThreadExecutor;
import com.webinfotech.meatration.domain.models.Charges;
import com.webinfotech.meatration.domain.models.testing.Cart;
import com.webinfotech.meatration.presentation.presenters.CartDetailsPresenter;
import com.webinfotech.meatration.presentation.presenters.impl.CartDetailsPresenterImpl;
import com.webinfotech.meatration.presentation.ui.adapters.CartListAdapter;
import com.webinfotech.meatration.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.meatration.presentation.ui.dialogs.DeliverySpeedDialog;
import com.webinfotech.meatration.threading.MainThreadImpl;

import java.util.ArrayList;

public class CartListActivity extends AppCompatActivity implements CartDetailsPresenter.View, DeliverySpeedDialog.Callback {

    @BindView(R.id.recycler_view_cart_list)
    RecyclerView recyclerViewCartList;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.txt_view_sub_total)
    TextView txtViewSubTotal;
    @BindView(R.id.txt_view_total)
    TextView txtViewTotal;
    @BindView(R.id.txt_view_discount)
    TextView txtViewDiscount;
    BottomSheetDialog addressBottomSheetDialog;
    TextView txtViewAddAddress;
    ImageView imgViewAddAddress;
    RecyclerView recyclerViewShippingAddress;
    CartDetailsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    DeliverySpeedDialog deliverySpeedDialog;
    Charges[] charges;
    double totalMrp;
    double totalAmount;
    double discount;
    double deliveryCharges;
    int shippingAddressId;
    int deliveryType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        getSupportActionBar().setTitle("Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setUpProgressDialog();
        setAddressBottomSheetDialog();
        setUpDeliverySpeedDialog();
        initialisePresenter();
    }

    public void initialisePresenter() {
        mPresenter = new CartDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setAddressBottomSheetDialog() {
        if (addressBottomSheetDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_address_bottom_sheet, null);
            addressBottomSheetDialog = new BottomSheetDialog(this);
            txtViewAddAddress = (TextView) view.findViewById(R.id.txt_view_add_address);
            imgViewAddAddress = (ImageView) view.findViewById(R.id.img_view_add_address);
            recyclerViewShippingAddress = (RecyclerView) view.findViewById(R.id.recycler_view_address);
            txtViewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
                    startActivity(intent);
                }
            });
            addressBottomSheetDialog.setContentView(view);
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setUpDeliverySpeedDialog() {
        deliverySpeedDialog = new DeliverySpeedDialog(this, this, this);
        deliverySpeedDialog.setUpDialogView();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadCartAdapter(CartListAdapter adapter, double totalAmount, double discount, double subTotal) {
        mainLayout.setVisibility(View.VISIBLE);
        recyclerViewCartList.setAdapter(adapter);
        recyclerViewCartList.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewCartList.addItemDecoration(itemDecor);

        txtViewSubTotal.setText("Rs. " + subTotal);
        txtViewTotal.setText("Rs. " + totalAmount);
        txtViewDiscount.setText("Rs. " + discount);

        totalMrp = subTotal;
        this.totalAmount = totalAmount;
        this.discount = discount;

    }

    @Override
    public void loadShippingAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter) {
        recyclerViewShippingAddress.setAdapter(cartShippingAddressAdapter);
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewShippingAddress.addItemDecoration(itemDecor);
    }

    @Override
    public void onShippingAddressEditClicked(int addressId) {
        Intent intent = new Intent(this, EditAddressActivity.class);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @Override
    public void onAddressSelected(int addressId) {
        shippingAddressId = addressId;
    }

    @Override
    public void goToPaymentActivity() {
        deliverySpeedDialog.showDialog();
    }

    @Override
    public void loadCharges(Charges[] charges) {
        this.charges = charges;
        deliverySpeedDialog.loadCharges(charges);
    }

    @Override
    public void showErrorMessage(String errorMsg) {

    }

    @Override
    public void hideViews() {
        mainLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideExpressDeliveryOption() {
        deliverySpeedDialog.hideExpressDelivery();
    }

    @OnClick(R.id.btn_select_delivery_address) void onSetDeliveryAddressClicked() {
        addressBottomSheetDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartDetails();
        mPresenter.fetchChargesList();
        mPresenter.fetchShippingAddressList();
    }

    @Override
    public void onProccedClicked() {
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra("totalMrp", totalMrp);
        intent.putExtra("totalAmount", totalAmount);
        intent.putExtra("discount", discount);
        intent.putExtra("deliveryCharges", deliveryCharges);
        intent.putExtra("deliveryType", deliveryType);
        intent.putExtra("shippingAddressId", shippingAddressId);
        startActivity(intent);
    }

    @Override
    public void onDeliveryOptionsSelected(int id) {
        if (id == 1) {
            deliveryCharges = charges[0].amount;
            deliveryType = 1;
        } else {
            deliveryCharges = charges[1].amount;
            deliveryType = 2;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}