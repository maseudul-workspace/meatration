package com.webinfotech.meatration.domain.interactors;

public interface RegisterUserInteractor {
    interface Callback {
        void onRegisterSuccess();
        void onRegisterFail(String errorMsg);
    }
}
