package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.Subcategory;

public interface FetchSubcategoriesInteractor {
    interface Callback {
        void onGettingSubcategoriesSuccess(Subcategory[] subcategories);
        void onGettingSubcategoriesFail(String errorMsg);
    }
}
