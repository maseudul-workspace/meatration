package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.Product;

public interface FetchProductListInteractor {
    interface Callback {
        void onGettingProductListSuccess(Product[] products, int totalPage);
        void onGettingProductListFail(String errorMsg);
    }
}
