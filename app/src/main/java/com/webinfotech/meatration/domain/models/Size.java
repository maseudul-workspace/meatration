package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Size {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("size_type_id")
    @Expose
    public int sizeTypeId;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("size")
    @Expose
    public int size;

    @SerializedName("mrp")
    @Expose
    public double mrp;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("min_ord_quantity")
    @Expose
    public int minOrderQuantity;

    @SerializedName("stock")
    @Expose
    public int stock;

    public boolean isSelected = false;

}
