package com.webinfotech.meatration.domain.models.testing;

public class Size {

    public int id;
    public String name;
    public boolean isSelected;

    public Size(int id, String name, boolean isSelected) {
        this.id = id;
        this.name = name;
        this.isSelected = isSelected;
    }
}
