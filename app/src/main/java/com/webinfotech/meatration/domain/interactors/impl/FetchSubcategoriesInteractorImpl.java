package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchSubcategoriesInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.HomeData;
import com.webinfotech.meatration.domain.models.Subcategory;
import com.webinfotech.meatration.domain.models.SubcategoryWrapper;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class FetchSubcategoriesInteractorImpl extends AbstractInteractor implements FetchSubcategoriesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int categoryId;

    public FetchSubcategoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int categoryId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.categoryId = categoryId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategoriesFail(errorMsg);
            }
        });
    }

    private void postMessage(Subcategory[] subcategories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategoriesSuccess(subcategories);
            }
        });
    }

    @Override
    public void run() {
        SubcategoryWrapper subcategoryWrapper = mRepository.fetchSubcategories(categoryId);
        if (subcategoryWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!subcategoryWrapper.status) {
            notifyError(subcategoryWrapper.message);
        } else {
            postMessage(subcategoryWrapper.subcategories);
        }
    }
}
