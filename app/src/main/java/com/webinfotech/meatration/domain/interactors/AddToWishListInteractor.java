package com.webinfotech.meatration.domain.interactors;

public interface AddToWishListInteractor {
    interface Callback {
        void onAddToWishListSuccess();
        void onAddToWishListFail(String errorMsg, int loginError);
    }
}
