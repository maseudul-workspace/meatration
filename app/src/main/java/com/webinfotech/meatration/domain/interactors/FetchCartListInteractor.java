package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.CartList;

public interface FetchCartListInteractor {
    interface Callback {
        void onGettingCartListSuccess(CartList[] cartLists);
        void onGettingCartListFail(String errorMsg, int loginError);
    }
}
