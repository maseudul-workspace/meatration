package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderProducts {

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("product_name")
    @Expose
    public String productName;

    @SerializedName("product_type")
    @Expose
    public int productType;

    @SerializedName("size")
    @Expose
    public String size;

    @SerializedName("quantity")
    @Expose
    public int quantity;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("product_image")
    @Expose
    public String productImage;

}
