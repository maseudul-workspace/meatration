package com.webinfotech.meatration.domain.models.testing;

public class Cart {

    public String name;
    public String image;
    public String size;
    public String price;
    public String quantity;

    public Cart(String name, String image, String size, String price, String quantity) {
        this.name = name;
        this.image = image;
        this.size = size;
        this.price = price;
        this.quantity = quantity;
    }
}
