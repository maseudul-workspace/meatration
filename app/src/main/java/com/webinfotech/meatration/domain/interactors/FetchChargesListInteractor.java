package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.Charges;

public interface FetchChargesListInteractor {
    interface Callback {
        void onChargesListFetchSuccess(Charges[] charges);
        void onChargesListFetchFail();
    }
}
