package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeData {

    @SerializedName("slider")
    @Expose
    public Image[] sliders;

    @SerializedName("category")
    @Expose
    public Category[] categories;

    @SerializedName("popular_products")
    @Expose
    public Product[] popularProducts;

    @SerializedName("new_arrivals")
    @Expose
    public Product[] newProducts;

}
