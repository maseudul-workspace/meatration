package com.webinfotech.meatration.domain.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailsData {

    @SerializedName("product_details")
    @Expose
    public Product product;

    @SerializedName("related_data")
    @Expose
    public RelatedData relatedData;

}
