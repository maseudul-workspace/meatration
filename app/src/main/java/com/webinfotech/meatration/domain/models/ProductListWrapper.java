package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductListWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("current_page")
    @Expose
    public int currentPage;

    @SerializedName("total_page")
    @Expose
    public int totalPage;

    @SerializedName("data")
    @Expose
    public Product[] products;

}
