package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChargesListWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;

    @SerializedName("data")
    @Expose
    public Charges[] charges;

}
