package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.HomeData;

public interface FetchHomeDataInteractor {
    interface Callback {
        void onGettingHomeDataSuccess(HomeData homeData);
        void onGettingHomeDataFail(String errorMsg);
    }
}
