package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.AddShippingAddressInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.CommonResponse;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

import retrofit2.http.Field;

public class AddShippingAddressInteractorImpl extends AbstractInteractor implements AddShippingAddressInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;
    String name;
    String email;
    String mobile;
    String state;
    String city;
    String pin;
    String address;
    double latitude;
    double longitude;

    public AddShippingAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken, String name, String email, String mobile, String state, String city, String pin, String address,  double latitude, double longitude) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.state = state;
        this.city = city;
        this.pin = pin;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddShippingAddressFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddShippingAddressSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.addShippingAddress(apiToken, userId, name, email, mobile, state, city, pin, address, latitude, longitude);
        if (commonResponse == null) {
            notifyError("Slow Internet Connection", commonResponse.login_error);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
