package com.webinfotech.meatration.domain.interactors;

public interface UpdateAddressInteractor {
    interface Callback {
        void onUpdateAddressSuccess();
        void onUpdateAddressFail(String errorMsg, int loginError);
    }
}
