package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
