package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.WishList;

public interface FetchWishListInteractor {
    interface Callback {
        void onWishListFetchSuccess(WishList[] wishLists);
        void onWishListFetchFail(String errorMsg, int loginError);
    }
}
