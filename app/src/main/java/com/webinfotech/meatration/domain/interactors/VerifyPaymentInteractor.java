package com.webinfotech.meatration.domain.interactors;

public interface VerifyPaymentInteractor {
    interface Callback {
        void onVerifyPaymentSuccess();
        void onVerifyPaymentFail(String errorMsg);
    }
}
