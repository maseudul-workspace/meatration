package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.UserInfo;
import com.webinfotech.meatration.domain.models.UserInfoWrapper;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class FetchUserProfileInteractorImpl extends AbstractInteractor implements FetchUserProfileInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;

    public FetchUserProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserProfileFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserProfileSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.fetchUserProfile(apiKey, userId);
        if (userInfoWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message, userInfoWrapper.login_error);
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
