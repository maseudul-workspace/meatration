package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.ShippingAddress;

public interface FetchShippingAddressDetailsInteractor {
    interface Callback {
        void onGettingAddressDetailsSuccess(ShippingAddress shippingAddress);
        void onGettingAddressDetailsFail(String errorMsg, int loginError);
    }
}
