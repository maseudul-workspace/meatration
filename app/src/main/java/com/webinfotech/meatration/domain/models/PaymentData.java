package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentData {

    @SerializedName("key_id")
    @Expose
    public String keyId;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("order_id")
    @Expose
    public String order_id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

}
