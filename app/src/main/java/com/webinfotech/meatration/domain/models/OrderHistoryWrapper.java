package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderHistoryWrapper {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("current_page")
    @Expose
    public int currentPage;

    @SerializedName("total_page")
    @Expose
    public int total_page;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;

    @SerializedName("data")
    @Expose
    public Order[] orders;

}
