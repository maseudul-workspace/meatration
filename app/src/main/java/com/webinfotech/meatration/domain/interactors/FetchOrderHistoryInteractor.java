package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.Order;

public interface FetchOrderHistoryInteractor {
    interface Callback {
        void onGettingOrderHistorySuccess(Order[] orders, int totalPage);
        void onGettingOrderHistoryFail(String errorMsg, int loginError);
    }
}
