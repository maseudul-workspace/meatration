package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.ShippingAddress;

public interface FetchShippingAddressInteractor {
    interface Callback {
        void onGettingAddressListSucces(ShippingAddress[] shippingAddresses);
        void onGettingAddressListFail(String errorMsg, int loginError);
    }
}
