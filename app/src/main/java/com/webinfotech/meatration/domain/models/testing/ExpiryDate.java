package com.webinfotech.meatration.domain.models.testing;

public class ExpiryDate {

    public int id;
    public String appearedDate;
    public String exactDate;

    public ExpiryDate(int id, String appearedDate, String exactDate) {
        this.id = id;
        this.appearedDate = appearedDate;
        this.exactDate = exactDate;
    }
}
