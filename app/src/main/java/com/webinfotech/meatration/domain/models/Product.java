package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("main_image")
    @Expose
    public String mainImage;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("min_price")
    @Expose
    public String minPrice;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("product_type")
    @Expose
    public int productType;

    @SerializedName("sizes")
    @Expose
    public Size[] sizes;

    @SerializedName("images")
    @Expose
    public Image[] images;

    public boolean isWishListed = false;

}
