package com.webinfotech.meatration.domain.interactors;

public interface AddShippingAddressInteractor {
    interface Callback {
        void onAddShippingAddressSuccess();
        void onAddShippingAddressFail(String errorMsg, int loginError);
    }
}
