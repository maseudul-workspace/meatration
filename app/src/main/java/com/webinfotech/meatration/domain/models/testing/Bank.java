package com.webinfotech.meatration.domain.models.testing;

public class Bank {

    public String bankCode;
    public String bankName;

    public Bank(String bankCode, String bankName) {
        this.bankCode = bankCode;
        this.bankName = bankName;
    }
}
