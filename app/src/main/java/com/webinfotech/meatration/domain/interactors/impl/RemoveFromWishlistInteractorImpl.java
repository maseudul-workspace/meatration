package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.RemoveFromWishlistInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.CommonResponse;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class RemoveFromWishlistInteractorImpl extends AbstractInteractor implements RemoveFromWishlistInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int wishListId;

    public RemoveFromWishlistInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int wishListId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.wishListId = wishListId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishListRemoveFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishListRemoveSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.removeFromWishlist(apiToken, wishListId);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
