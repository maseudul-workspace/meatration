package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RelatedData {

    @SerializedName("choosed_data")
    @Expose
    public Product[] choosedProducts;

    @SerializedName("category_related")
    @Expose
    public Product[] relatedProducts;

}
