package com.webinfotech.meatration.domain.models.testing;

public class WalletList {

    public String walletCode;
    public boolean isSelected;

    public WalletList(String walletCode, boolean isSelected) {
        this.walletCode = walletCode;
        this.isSelected = isSelected;
    }
}
