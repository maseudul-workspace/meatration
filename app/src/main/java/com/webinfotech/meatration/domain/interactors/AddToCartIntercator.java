package com.webinfotech.meatration.domain.interactors;

public interface AddToCartIntercator {
    interface Callback {
        void onAddToCartSuccess();
        void onAddToCartFail(String errorMsg, int loginError);
    }
}
