package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 15-02-2019.
 */

public class ShippingAddressWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;

    @SerializedName("data")
    @Expose
    public ShippingAddress[] shippingAddresses;

}
