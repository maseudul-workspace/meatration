package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.DeleteShippingAddressInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.CommonResponse;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class DeleteShippingAddressInteractorImpl extends AbstractInteractor implements DeleteShippingAddressInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int addressId;
    String apiToken;

    public DeleteShippingAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int addressId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.addressId = addressId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.deleteShippingAddress(apiToken, addressId);
        if (commonResponse == null) {
            notifyError("Slow Internet Connection", commonResponse.login_error);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
