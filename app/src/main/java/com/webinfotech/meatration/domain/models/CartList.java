package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartList {

    @SerializedName("cart_id")
    @Expose
    public int cartId;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("quantity")
    @Expose
    public int quantity;

    @SerializedName("size_id")
    @Expose
    public int sizeId;

    @SerializedName("product")
    @Expose
    public Product product;

}
