package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.RegisterUserInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.CommonResponse;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class RegisterUserInteractorImpl extends AbstractInteractor implements RegisterUserInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String email;
    String mobile;
    String DOB;
    String gender;
    String state;
    String city;
    String pin;
    String address;
    String password;
    String confirmPassword;

    public RegisterUserInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String name, String email, String mobile, String DOB, String gender, String state, String city, String pin, String address, String password, String confirmPassword) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.DOB = DOB;
        this.gender = gender;
        this.state = state;
        this.city = city;
        this.pin = pin;
        this.address = address;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.registerUser(name, email, mobile, DOB, gender, state, city, pin, address, password, confirmPassword);
        if (commonResponse == null) {
            notifyError("Something Went Wrong");
        } else if (!commonResponse.status) {
            if (commonResponse.error) {
                if (commonResponse.errorMesage.email != null) {
                    notifyError("Email Already Taken");
                } else if (commonResponse.errorMesage.mobile != null) {
                    notifyError("Mobile No Already Taken");
                } else {
                    notifyError(commonResponse.message);
                }
            } else {
                notifyError(commonResponse.message);
            }
        } else {
            postMessage();
        }
    }
}
