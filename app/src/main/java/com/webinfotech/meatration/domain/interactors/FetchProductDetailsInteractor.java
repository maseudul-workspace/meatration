package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.Product;
import com.webinfotech.meatration.domain.models.ProductDetailsData;
import com.webinfotech.meatration.presentation.ui.activities.ProductDetailsActivity;

public interface FetchProductDetailsInteractor {
    interface Callback {
        void onGettingProductDetailsSuccess(ProductDetailsData productDetailsData);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
