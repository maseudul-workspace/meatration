package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("order_id")
    @Expose
    public int orderId;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("shipping_charge")
    @Expose
    public String shippingCharges;

    @SerializedName("total_amount")
    @Expose
    public String totalAmount;

    @SerializedName("order_type")
    @Expose
    public int orderType;

    @SerializedName("delivery_type")
    @Expose
    public int deliveryType;

    @SerializedName("payment_type")
    @Expose
    public int paymentType;

    @SerializedName("payment_status")
    @Expose
    public int paymentStatus;

    @SerializedName("delivery_status")
    @Expose
    public int deliveryStatus;

    @SerializedName("is_refund")
    @Expose
    public int isRefund;

    @SerializedName("items")
    @Expose
    public OrderProducts[] orderProducts;

    @SerializedName("date")
    @Expose
    public String date;

}
