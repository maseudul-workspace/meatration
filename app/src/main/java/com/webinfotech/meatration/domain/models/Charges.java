package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Charges {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("amount")
    @Expose
    public double amount;

}
