package com.webinfotech.meatration.domain.models.testing;

public class Product {

    public int id;
    public String name;
    public String image;
    public String price;
    public String mrp;

    public Product(int id, String name, String image, String price, String mrp) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.price = price;
        this.mrp = mrp;
    }

}
