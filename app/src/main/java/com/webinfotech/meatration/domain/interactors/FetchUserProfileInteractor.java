package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.UserInfo;

public interface FetchUserProfileInteractor {
    interface Callback {
        void onGettingUserProfileSuccess(UserInfo userInfo);
        void onGettingUserProfileFail(String errorMsg, int loginError);
    }
}
