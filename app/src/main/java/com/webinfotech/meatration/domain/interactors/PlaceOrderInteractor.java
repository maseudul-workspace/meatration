package com.webinfotech.meatration.domain.interactors;

import com.webinfotech.meatration.domain.models.PaymentDataMain;

public interface PlaceOrderInteractor {
    interface Callback {
        void onPlaceOrderSuccess(PaymentDataMain paymentDataMain);
        void onPlaceOrderFail(String errorMsg, int loginError);
    }
}
