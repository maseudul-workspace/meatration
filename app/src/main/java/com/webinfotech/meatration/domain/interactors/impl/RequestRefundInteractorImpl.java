package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.RequestRefundInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.CommonResponse;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class RequestRefundInteractorImpl extends AbstractInteractor implements RequestRefundInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int orderId;
    String name;
    String bankName;
    String branchName;
    String accountName;
    String ifsc;

    public RequestRefundInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int orderId, String name, String bankName, String branchName, String accountName, String ifsc) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.orderId = orderId;
        this.name = name;
        this.bankName = bankName;
        this.accountName = accountName;
        this.ifsc = ifsc;
        this.branchName = branchName;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRequestRefundFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRequestRefundSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.requestRefund(authorization, orderId, name, bankName, branchName, accountName, ifsc);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
