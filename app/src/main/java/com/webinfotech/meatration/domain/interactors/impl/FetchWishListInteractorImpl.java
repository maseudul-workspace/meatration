package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchWishListInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.ShippingAddress;
import com.webinfotech.meatration.domain.models.WishList;
import com.webinfotech.meatration.domain.models.WishlistWrapper;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class FetchWishListInteractorImpl extends AbstractInteractor implements FetchWishListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchWishListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishListFetchFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(WishList[] wishLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWishListFetchSuccess(wishLists);
            }
        });
    }

    @Override
    public void run() {
        final WishlistWrapper wishlistWrapper = mRepository.fetchWishList(apiToken, userId);
        if (wishlistWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!wishlistWrapper.status) {
            notifyError(wishlistWrapper.message, wishlistWrapper.login_error);
        } else {
            postMessage(wishlistWrapper.wishLists);
        }
    }
}
