package com.webinfotech.meatration.domain.models.testing;

public class Address {

    public String userName;
    public String address;
    public String mobile;
    public String houseNo;
    public String street;

    public Address(String userName, String address, String mobile, String houseNo, String street) {
        this.userName = userName;
        this.address = address;
        this.mobile = mobile;
        this.houseNo = houseNo;
        this.street = street;
    }
}
