package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.SendOtpInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.OtpResponse;
import com.webinfotech.meatration.repository.AppRepository;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class SendOtpInteractorImpl extends AbstractInteractor implements SendOtpInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String phone;

    public SendOtpInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String phone) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.phone = phone;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendOtpFail(errorMsg);
            }
        });
    }

    private void postMessage(String otp){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendOtpSuccess(otp);
            }
        });
    }

    @Override
    public void run() {
        final OtpResponse otpResponse = mRepository.sendOtp(phone);
        if (otpResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!otpResponse.status) {
            notifyError(otpResponse.message);
        } else {
            postMessage(otpResponse.otpData.otp);
        }
    }
}
