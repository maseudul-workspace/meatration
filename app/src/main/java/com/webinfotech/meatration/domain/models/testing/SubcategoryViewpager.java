package com.webinfotech.meatration.domain.models.testing;

public class SubcategoryViewpager {

    public String image;
    public String longDesc;
    public String shortDesc;

    public SubcategoryViewpager(String image, String longDesc, String shortDesc) {
        this.image = image;
        this.longDesc = longDesc;
        this.shortDesc = shortDesc;
    }
}
