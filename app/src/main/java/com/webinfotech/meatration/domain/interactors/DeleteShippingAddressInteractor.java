package com.webinfotech.meatration.domain.interactors;

public interface DeleteShippingAddressInteractor {
    interface Callback {
        void onAddressDeleteSuccess();
        void onAddressDeleteFail(String errorMsg, int loginError);
    }
}
