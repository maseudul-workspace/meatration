package com.webinfotech.meatration.domain.models.testing;

public class Category {

    public int id;
    public String name;
    public String image;

    public Category(int id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

}
