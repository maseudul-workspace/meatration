package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.ShippingAddress;
import com.webinfotech.meatration.domain.models.ShippingAddressWrapper;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class FetchShippingAddressInteractorImpl extends AbstractInteractor implements FetchShippingAddressInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchShippingAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(ShippingAddress[] shippingAddresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressListSucces(shippingAddresses);
            }
        });
    }

    @Override
    public void run() {
        final ShippingAddressWrapper shippingAddressWrapper = mRepository.fetchShippingAddress(apiToken, userId);
        if (shippingAddressWrapper == null) {
            notifyError("Slow Internet Connection", shippingAddressWrapper.login_error);
        } else if (!shippingAddressWrapper.status) {
            notifyError(shippingAddressWrapper.message, shippingAddressWrapper.login_error);
        } else {
            postMessage(shippingAddressWrapper.shippingAddresses);
        }
    }
}
