package com.webinfotech.meatration.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtpData {

    @SerializedName("otp")
    @Expose
    public String otp;

}
