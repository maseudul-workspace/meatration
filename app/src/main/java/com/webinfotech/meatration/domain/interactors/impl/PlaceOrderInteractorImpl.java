package com.webinfotech.meatration.domain.interactors.impl;

import com.webinfotech.meatration.domain.executors.Executor;
import com.webinfotech.meatration.domain.executors.MainThread;
import com.webinfotech.meatration.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.meatration.domain.interactors.base.AbstractInteractor;
import com.webinfotech.meatration.domain.models.CommonResponse;
import com.webinfotech.meatration.domain.models.OrderPlaceResponse;
import com.webinfotech.meatration.domain.models.PaymentDataMain;
import com.webinfotech.meatration.repository.AppRepositoryImpl;

public class PlaceOrderInteractorImpl extends AbstractInteractor implements PlaceOrderInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int deliveryType;
    int paymentType;
    int shippingAddressId;

    public PlaceOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int deliveryType, int paymentType, int shippingAddressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.deliveryType = deliveryType;
        this.paymentType = paymentType;
        this.shippingAddressId = shippingAddressId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(PaymentDataMain paymentDataMain){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderSuccess(paymentDataMain);
            }
        });
    }

    @Override
    public void run() {
        final OrderPlaceResponse orderPlaceResponse = mRepository.placeOrder(apiToken, userId, deliveryType, paymentType, shippingAddressId);
        if (orderPlaceResponse == null) {
            notifyError("Please Internet Connection", 0);
        } else if (!orderPlaceResponse.status) {
            notifyError(orderPlaceResponse.message, orderPlaceResponse.login_error);
        } else {
            postMessage(orderPlaceResponse.paymentDataMain);
        }
    }
}
