package com.webinfotech.meatration.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AppRepository {

    @POST("user/registration")
    @FormUrlEncoded
    Call<ResponseBody> registerUser(@Field("name") String name,
                                    @Field("email") String email,
                                    @Field("mobile") String mobile,
                                    @Field("dob") String DOB,
                                    @Field("gender") String gender,
                                    @Field("state") String state,
                                    @Field("city") String city,
                                    @Field("pin") String pin,
                                    @Field("address") String address,
                                    @Field("password") String password,
                                    @Field("confirm_password") String confirmPassword
    );

    @POST("user/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("user_id") String userId,
                                  @Field("password") String password
    );

    @GET("user/profile/{user_id}")
    Call<ResponseBody> fetchUserProfile(@Header("Authorization") String authorization,
                                        @Path("user_id") int userId
    );

    @POST("user/profile/update")
    @FormUrlEncoded
    Call<ResponseBody> updateUser(@Header("Authorization") String authorization,
                                  @Field("user_id") int userId,
                                  @Field("name") String name,
                                  @Field("email") String email,
                                  @Field("mobile") String mobile,
                                  @Field("dob") String DOB,
                                  @Field("gender") String gender,
                                  @Field("state") String state,
                                  @Field("city") String city,
                                  @Field("pin") String pin,
                                  @Field("address") String address
    );

    @GET("user/shipping/list/{user_id}")
    Call<ResponseBody> fetchShippingAddressList(@Header("Authorization") String authorization,
                                                @Path("user_id") int userId
    );

    @POST("user/shipping/add")
    @FormUrlEncoded
    Call<ResponseBody> addShippingAddress(@Header("Authorization") String authorization,
                                          @Field("user_id") int userId,
                                          @Field("name") String name,
                                          @Field("email") String email,
                                          @Field("mobile") String mobile,
                                          @Field("state") String state,
                                          @Field("city") String city,
                                          @Field("pin") String pin,
                                          @Field("address") String address,
                                          @Field("latitude") double latitude,
                                          @Field("longtitude") double longitude
    );

    @GET("user/shipping/single/{user_id}/{address_id}")
    Call<ResponseBody> fetchShippingAddressDetails(@Header("Authorization") String authorization,
                                                   @Path("user_id") int userId,
                                                   @Path("address_id") int addressId
    );

    @POST("user/shipping/update")
    @FormUrlEncoded
    Call<ResponseBody> updateShippingAddress(@Header("Authorization") String authorization,
                                             @Field("user_id") int userId,
                                             @Field("address_id") int addressId,
                                             @Field("name") String name,
                                             @Field("email") String email,
                                             @Field("mobile") String mobile,
                                             @Field("state") String state,
                                             @Field("city") String city,
                                             @Field("pin") String pin,
                                             @Field("address") String address,
                                             @Field("latitude") double latitude,
                                             @Field("longtitude") double longitude
    );

    @GET("user/shipping/delete/{address_id}")
    Call<ResponseBody> deleteShippingAddress(@Header("Authorization") String authorization,
                                             @Path("address_id") int addressId
    );

    @POST("user/change/password")
    @FormUrlEncoded
    Call<ResponseBody> changePassword(@Header("Authorization") String authorization,
                                      @Field("user_id") int userId,
                                      @Field("current_pass") String currentPassword,
                                      @Field("new_password") String newPassword,
                                      @Field("confirm_password") String confirmPassword
    );

    @GET("app/load")
    Call<ResponseBody> fetchHomeData();

    @GET("sub/category/list/{category_id}")
    Call<ResponseBody> fetchSubcategories(@Path("category_id") int categoryId);

    @GET("product/list/{category_id}/{type}/{page}")
    Call<ResponseBody> fetchProductList(@Path("category_id") int categoryId,
                                          @Path("type") int type,
                                          @Path("page") int page
                                          );

    @GET("product/product/single/view/{product_id}")
    Call<ResponseBody> fetchProductDetails(@Path("product_id") int productId);

    @POST("user/cart/add")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(@Header("Authorization") String authorization,
                                 @Field("user_id") int userId,
                                 @Field("product_id") int productId,
                                 @Field("quantity") String qty,
                                 @Field("size_id") String sizeId
    );

    @GET("user/cart/fetch/product/{user_id}")
    Call<ResponseBody> fetchCartList(@Header("Authorization") String authorization,
                                     @Path("user_id") int user_id);

    @GET("user/cart/update/quantity/{cart_id}/{quantity}")
    Call<ResponseBody> updateCart(@Header("Authorization") String authorization,
                                  @Path("cart_id") int cartId,
                                  @Path("quantity") int quantity
                                  );

    @GET("user/cart/remove/item/{cart_id}")
    Call<ResponseBody> deleteCart(@Header("Authorization") String authorization,
                                  @Path("cart_id") int cartId
    );

    @GET("charges/list")
    Call<ResponseBody> fetchChargesList();

    @GET("user/wish/list/add/{product_id}/{user_id}")
    Call<ResponseBody> addToWishList(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId,
                                     @Path("product_id") int productId
    );

    @GET("user/wish/list/items/{user_id}")
    Call<ResponseBody> fetchWishList(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId
    );

    @GET("user/wish/list/item/remove/{wish_list_id}")
    Call<ResponseBody> removeFromWishList(  @Header("Authorization") String authorization,
                                            @Path("wish_list_id") int wishListId
    );

    @POST("user/place/order")
    @FormUrlEncoded
    Call<ResponseBody> placeOrder(@Header("Authorization") String authorization,
                                  @Field("user_id") int userId,
                                  @Field("delivery_type") int deliveryType,
                                  @Field("payment_type") int paymentType,
                                  @Field("shipping_address_id") int shippingAddressId
    );

    @GET("user/order/history/{user_id}/{page}")
    Call<ResponseBody> fetchOrderList(  @Header("Authorization") String authorization,
                                        @Path("user_id") int userId,
                                        @Path("page") int page
    );

    @GET("user/order/cancel/{order_id}")
    Call<ResponseBody> cancelOrder(  @Header("Authorization") String authorization,
                                     @Path("order_id") int orderId
    );

    @POST("user/order/refund/info/insert")
    @FormUrlEncoded
    Call<ResponseBody> requestRefund(   @Header("Authorization") String authorization,
                                        @Field("order_id") int orderId,
                                        @Field("name") String name,
                                        @Field("bank_name") String bankName,
                                        @Field("branch_name") String branchName,
                                        @Field("ac_no") String accountName,
                                        @Field("ifsc") String ifsc
    );

    @GET("send/otp/{mobile}")
    Call<ResponseBody> sendOtp(@Path("mobile") String mobile);

    @POST("user/order/payment/verify")
    @FormUrlEncoded
    Call<ResponseBody> verifyPayment(   @Header("Authorization") String authorization,
                                        @Field("user_id") int userId,
                                        @Field("razorpay_order_id") String razorpayOrderId,
                                        @Field("razorpay_payment_id") String razorpayPaymentId,
                                        @Field("razorpay_signature") String razorpaySignature,
                                        @Field("order_id") int orderId
    );

}
