package com.webinfotech.meatration.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.meatration.domain.models.CartListWrapper;
import com.webinfotech.meatration.domain.models.ChargesListWrapper;
import com.webinfotech.meatration.domain.models.CommonResponse;
import com.webinfotech.meatration.domain.models.HomeDataWrapper;
import com.webinfotech.meatration.domain.models.OrderHistoryWrapper;
import com.webinfotech.meatration.domain.models.OrderPlaceResponse;
import com.webinfotech.meatration.domain.models.OtpResponse;
import com.webinfotech.meatration.domain.models.ProductDetailsWrapper;
import com.webinfotech.meatration.domain.models.ProductListWrapper;
import com.webinfotech.meatration.domain.models.ShippingAddressDetailsWrapper;
import com.webinfotech.meatration.domain.models.ShippingAddressWrapper;
import com.webinfotech.meatration.domain.models.Subcategory;
import com.webinfotech.meatration.domain.models.SubcategoryWrapper;
import com.webinfotech.meatration.domain.models.UserInfoWrapper;
import com.webinfotech.meatration.domain.models.WishlistWrapper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public CommonResponse registerUser(String name,
                                       String email,
                                       String mobile,
                                       String DOB,
                                       String gender,
                                       String state,
                                       String city,
                                       String pin,
                                       String address,
                                       String password,
                                       String confirmPassword
                                        ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.registerUser(name, email, mobile, DOB, gender, state, city, pin, address, password, confirmPassword);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserInfoWrapper checkLogin(String phone, String password){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(phone, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public UserInfoWrapper fetchUserProfile(String apiToken, int userId){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUserProfile("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public CommonResponse updateUser(  String apiToken,
                                       int userId,
                                       String name,
                                       String email,
                                       String mobile,
                                       String DOB,
                                       String gender,
                                       String state,
                                       String city,
                                       String pin,
                                       String address
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateUser("Bearer " + apiToken, userId, name, email, mobile, DOB, gender, state, city, pin, address);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public ShippingAddressWrapper fetchShippingAddress(String apiToken, int userId){
        ShippingAddressWrapper shippingAddressWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressList("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    shippingAddressWrapper = null;
                }else{
                    shippingAddressWrapper = gson.fromJson(responseBody, ShippingAddressWrapper.class);
                }
            } else {
                shippingAddressWrapper = null;
            }
        }catch (Exception e){
            shippingAddressWrapper = null;
        }
        return shippingAddressWrapper;
    }

    public CommonResponse addShippingAddress(  String apiToken,
                                               int userId,
                                               String name,
                                               String email,
                                               String mobile,
                                               String state,
                                               String city,
                                               String pin,
                                               String address,
                                               double latitude,
                                               double longitude
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addShippingAddress("Bearer " + apiToken, userId, name, email, mobile, state, city, pin, address, latitude, longitude);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public ShippingAddressDetailsWrapper fetchShippingAddressDetails(String apiToken, int userId, int addressId){
        ShippingAddressDetailsWrapper shippingAddressWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressDetails("Bearer " + apiToken, userId, addressId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    shippingAddressWrapper = null;
                }else{
                    shippingAddressWrapper = gson.fromJson(responseBody, ShippingAddressDetailsWrapper.class);
                }
            } else {
                shippingAddressWrapper = null;
            }
        }catch (Exception e){
            shippingAddressWrapper = null;
        }
        return shippingAddressWrapper;
    }

    public CommonResponse updateShippingAddress(  String apiToken,
                                                  int userId,
                                                  int addressId,
                                                  String name,
                                                  String email,
                                                  String mobile,
                                                  String state,
                                                  String city,
                                                  String pin,
                                                  String address,
                                                  double latitude,
                                                  double longitude
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateShippingAddress("Bearer " + apiToken, userId, addressId, name, email, mobile, state, city, pin, address, latitude, longitude);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Error Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            Log.e("LogMsg", e.getMessage());
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse deleteShippingAddress(String apiToken, int addressId){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> delete = mRepository.deleteShippingAddress("Bearer " + apiToken, addressId);
            Response<ResponseBody> response = delete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse changePassword(String apiToken, int userId, String currentPassword, String newPassword){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changePassword("Bearer " + apiToken, userId, currentPassword, newPassword, newPassword);
            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public HomeDataWrapper fetchHomeData() {
        HomeDataWrapper homeDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchHomeData();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    homeDataWrapper = null;
                }else{
                    homeDataWrapper = gson.fromJson(responseBody, HomeDataWrapper.class);
                }
            } else {
                homeDataWrapper = null;
            }
        }catch (Exception e){
            homeDataWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return homeDataWrapper;
    }

    public SubcategoryWrapper fetchSubcategories(int categoryId) {
        SubcategoryWrapper subcategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSubcategories(categoryId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    subcategoryWrapper = null;
                }else{
                    subcategoryWrapper = gson.fromJson(responseBody, SubcategoryWrapper.class);
                }
            } else {
                subcategoryWrapper = null;
            }
        }catch (Exception e){
            subcategoryWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return subcategoryWrapper;
    }

    public ProductListWrapper fetchProductList(int categoryId,int type, int page) {
        ProductListWrapper productListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchProductList(categoryId, type, page);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWrapper = null;
                }else{
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }
        }catch (Exception e){
            productListWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return productListWrapper;
    }

    public ProductDetailsWrapper fetchProductDetails(int productId) {
        ProductDetailsWrapper productDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchProductDetails(productId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productDetailsWrapper = null;
                }else{
                    productDetailsWrapper = gson.fromJson(responseBody, ProductDetailsWrapper.class);
                }
            } else {
                productDetailsWrapper = null;
            }
        }catch (Exception e){
            productDetailsWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return productDetailsWrapper;
    }

    public CommonResponse addToCart(String apiToken, int userId, int productId, String qty, String sizeId){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToCart("Bearer " + apiToken, userId, productId, qty, sizeId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CartListWrapper fetchCartList(String apiToken, int userId) {
        CartListWrapper cartListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCartList("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cartListWrapper = null;
                }else{
                    cartListWrapper = gson.fromJson(responseBody, CartListWrapper.class);
                }
            } else {
                cartListWrapper = null;
            }
        }catch (Exception e){
            cartListWrapper = null;
        }
        return cartListWrapper;
    }

    public CommonResponse updateCart(String apiToken, int cartId, int quantity) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateCart("Bearer " + apiToken, cartId, quantity);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse removeCartItem(String apiToken, int cartId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.deleteCart("Bearer " + apiToken, cartId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public ChargesListWrapper fetchChargesList() {
        ChargesListWrapper chargesListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchChargesList();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    chargesListWrapper = null;
                }else{
                    chargesListWrapper = gson.fromJson(responseBody, ChargesListWrapper.class);
                }
            } else {
                chargesListWrapper = null;
            }
        }catch (Exception e){
            chargesListWrapper = null;
        }
        return chargesListWrapper;
    }

    public CommonResponse addToWishList(String apiToken, int userId, int productId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToWishList("Bearer " + apiToken, userId, productId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public WishlistWrapper fetchWishList(String apiToken, int userId) {
        WishlistWrapper wishlistWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWishList("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    wishlistWrapper = null;
                }else{
                    wishlistWrapper = gson.fromJson(responseBody, WishlistWrapper.class);
                }
            } else {
                wishlistWrapper = null;
            }
        }catch (Exception e){
            wishlistWrapper = null;
        }
        return wishlistWrapper;
    }

    public CommonResponse removeFromWishlist(String apiToken, int wishListId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.removeFromWishList("Bearer " + apiToken, wishListId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OrderPlaceResponse placeOrder(String apiToken, int userId, int deliveryType, int paymentType, int shippingAddressId) {

        OrderPlaceResponse orderPlaceResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> place = mRepository.placeOrder("Bearer " + apiToken, userId, deliveryType, paymentType, shippingAddressId);
            Response<ResponseBody> response = place.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlaceResponse = null;
                }else{
                    orderPlaceResponse = gson.fromJson(responseBody, OrderPlaceResponse.class);
                }
            } else {
                orderPlaceResponse = null;
            }
        }catch (Exception e){
            orderPlaceResponse = null;
        }
        return orderPlaceResponse;
    }

    public OrderHistoryWrapper fetchOrderList(String apiToken, int userId, int page) {
        OrderHistoryWrapper orderHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchOrderList("Bearer " + apiToken, userId, page);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderHistoryWrapper = null;
                }else{
                    orderHistoryWrapper = gson.fromJson(responseBody, OrderHistoryWrapper.class);
                }
            } else {
                orderHistoryWrapper = null;
            }
        } catch (Exception e){
            orderHistoryWrapper = null;
        }
        return orderHistoryWrapper;
    }

    public CommonResponse cancelOrder(String apiToken, int orderId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> cancel = mRepository.cancelOrder("Bearer " + apiToken, orderId);
            Response<ResponseBody> response = cancel.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse requestRefund(String authorization,
                                        int orderId,
                                        String name,
                                        String bankName,
                                        String branchName,
                                        String accountName,
                                        String ifsc) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.requestRefund("Bearer " + authorization, orderId, name, bankName, branchName, accountName, ifsc);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OtpResponse sendOtp(String phone) {
        OtpResponse otpResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.sendOtp(phone);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    otpResponse = null;
                }else{
                    otpResponse = gson.fromJson(responseBody, OtpResponse.class);
                }
            } else {
                otpResponse = null;
            }
        }catch (Exception e){
            otpResponse = null;
        }
        return otpResponse;
    }

    public CommonResponse verifyPayment( String authorization,
                                  int userId,
                                  String razorpayOrderId,
                                  String razorpayPaymentId,
                                  String razorpaySignature,
                                  int orderId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.verifyPayment("Bearer " + authorization, userId, razorpayOrderId, razorpayPaymentId, razorpaySignature, orderId);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

}
